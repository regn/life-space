# Documentación de Life Space

Life Space Versión 1.5.0

## Change Log 

###(versión 1.1.0 -> 1.5.0)

Daniel Merrill:

+ Entidades ahora se mueven usando algoritmo A* de pathfinding

+ Zoom in (Z), Zoom out (X).

Cristobal Richard

+ Puede crearse un Robot de batalla al unir un tanque con plasma , este robot ataca y deja trampas cuando se mueve

+ Ahora las trampas pueden ser construidas por un milicia espacial mas un uranio

+ La funcion de distancia se recalculo de forma que tenga en cuenta la menor distancia en un tablero circular, ademas
se elimino la raiz cuadrada de la distancia.


###(versión 1.0.0 -> 1.1.0)

Daniel Merrill:

+ Migración de entidades

Cristóbal Richard:

+ Carga y guardado de juego apretando tecla S, L.

+ Cambio de nivel con teclas 1, 2 y 3.

Juan Pablo Rojas

+ Bugfix: Tanque no se agregaba si colisionaba con algo al transformarse el Marino Espacial.

###(versión 0.5.0 -> 1.0.0)

Cristobal Richard

+ Se cambio la clase Tablero builder agregando tres esenarios  de partidas distinos seleccionables en ejecucion
segun la dificultad.
+ Se crearon metodos en tablero builder para guardar y Cargar aunque todavia tienen bugs

Joaquín Torres:

+ Se creo la Clase ControladorOleadas para ingresar a los enemigos cada cierto tiempo al tablero

+ Se creo la Clase Madriguera para la salida de los enemigos al tablero

+ Se modificaron las clases CentroUrbano, Madriguera y Barracas para que salgan las entidades creadas cerca del edificio

Daniel Merrill:

+ Se arregló la entidad del aldeano para que se mueva y construya bien.

+ Se modificó la interfaz gráfica para que se refleje a quién se está atacando.

+ El usuario ahora puede poner recursos en el mapa. 

+ Sincronización de threads.
 
+ Mapa infinito (ahora es como un planeta, al llegar a un extremo volvemos al inicio).

+ Imagen del terreno con loop infinito.

+ Tests de JUnit.

Juan Pablo Rojas:

+ Se arregló definitivamente el tema de colisiones.

+ Se agregó nueva entidad (Tanque Espacial)

+ Ahora Marino Espacial se transforma en Tanque Espacial al interactuar con Acero.

###(versión 0.3.0 -> 0.5.0)

+ Se añadió la interfaz gráfica.

+ Se crearon las entidades.

+ Se programó la clase jugador, pero aún no se implementa.

## UML

El programa se basa en la siguiente representación en UML:

![Diagrama UML](https://bytebucket.org/regn/life-space/raw/e3a06afef9cc117fadcf1e7392ab6069630196b9/lifespaceuml.png)

## Clases importantes

### Tablero

Contiene la representación del tablero de juego. Se representa de la siguiente forma:

	 (0,0) +------------------+ (w,0)
	       |			   	  |
	       |				  |
	       | 				  |
	 (0,h) +------------------+ (w,h)

Atributos:

+ `private List<Entidad> entidades`: Lista de todas las entidades presentes en el mapa.

+ `private Celula[][] celulas`: Es la representación en celdas del tablero. Cada celda contiene una célula, que a su vez pertenece a una entidad.

Métodos:

+ `public void AgregarEntidad(Entidad ent)`: Llamar a este método cada vez que se quiera agregar una entidad al tablero. También agrega las células al arreglo de células.

+ `public void insertarPosicionEntidad(Entidad ent)` y `public void eliminarPosicionEntidad(Entidad ent)`: Modifican la posición de una entidad ya insertada en el tablero. Es útil cuando se requiere mover una entidad, ya que para esto hay que eliminarlo de la posición antigua y ponerlo en la siguiente.

+ `public List<Entidad> getEntidadesCercanas(Tuple2D<Integer, Integer> posDesde, double radio )`: Este método recibe una posición y un radio. Devuelve todas las entidades que se ubican en la circunferencia determinada por los parámetros. Útil cuando una entidad ataca o se mueve según lo que tiene en su perímetro.

+ `public void printEnConsola()`: Imprime la representación del tablero en consola.

### Entidad

Es una entidad abstracta.

Atributos: 

+ `protected long tiempoAccion`: Indica el tiempo que toma una entidad en realizar una acción en milisegundos.

+ `protected bool vivo`: Indica si la entidad está viva o muerta.

+ `public List<Celula> celulas`: Contiene todas las células que conforman la entidad.

+ `protected final int ticksParaMoverse`: Número de veces que hay que esperar a que transcurra `tiempoAccion` para que la entidad se mueva una vez.

+ `protected final int ticksInteractuar`: Lo mismo que el anterior pero para interactuar con otra entidad.

+ `protected  int cooldownMoverse`: Va llevando la cuenta de los ticks que han transcurrido. Una vez que llega a 0, vuelve a setearse en el valor de `ticksParaMoverse` y vuelve a comenzar el conteo.

+ `protected  int cooldownInteractuar`: Lo mismo que lo anterior pero para interactuar.

Métodos:

+ `Constructor`: Su deber es inicializar las variables y crear las células que conforman la entidad. Estas células deben crearse con posición relativa a (0,0). Ver la clase `Zergling` como referencia.

+ `protected void mover()`: Método que hay que sobrescribir para que cada entidad concreta lleve su lógica para moverse. Es importante hacer `super.mover()` en el método que sobrescriba esta función de la clase concreta para llevar a cabo la lógica de actualizar el cooldown.

+ `protected void interactuar()`: Método que hay que sobrescribir para que cada entidad concreta lleve su lógica para interactuar. Es importante hacer `interactuar.mover()` en el método que sobrescriba esta función de la clase concreta para llevar a cabo la lógica de actualizar el cooldown.

+ `public void run()`: Método que se ejecuta al lanzar el thread que corre la entidad. Itera eternamente mientras la entidad esté viva y llama a los métodos `mover()` e `interactuar()`. No se debería tocar este método ni llamarlo.

+ `public void start()`: Llamar a este método para comenzar a correr el thread de la entidad.

+ `protected Tuple2D<Integer,Integer> getCentroMasa()`: Calcula la posición central de la entidad. Útil en algunos casos.

+ `protected void moverHacia(Tuple2D<Integer, Integer> destino)`: Método auxiliar para que una entidad se mueva hacia una posición.

+ `public void posicionarInicialmente(Tuple2D<Integer, Integer> pos)`: Traslada las células de la entidad en el vector dado por pos.

### Clase Zergling

Actualmente (hasta la versión 0.3.0) es la única entidad completamente implementada. Por favor basarse en la implementación de esta clase para entender cómo programar otra entidad.

Métodos:

+ `Constructor`: Setea los parámetros iniciales y arma la estructura celular del Zergling.

+ `@Override protected void interactuar()`: Sobrescribe el método interactuar de la clase `entidad` y le añade lógica para atacar a un terrícola en su rango de ataque.

+ `@Override protected void mover()`: Sobrescribe el método mover de la clase `entidad` y le añade lógica para perseguir a un terrícola en su rango de visión.

### Clase Aumentador
  
Produce los recursos necesarios consumibles por el jugador

+ `Constructor` : Determina la estructura celular y , puntos de vida facciones , nivel de produccion en cada tik y cooldown de este 

+ `@Override protected void interactuar()` : La interaccion de  el aumentador en un tik que tik <=0  pide el jugador al tablero y agrega los recursos producidos a este


### Clase Aldeano

Enidad que puede crear entidades del tipo edificio al convinarce con recursos 

+ `Constructor` : determina estructura celular , facciones y puntos y caracteristicas

+ `@Override protected void mover()` : Se desplasa a el recurso mas cercano en su radio de  vision 

+ `@Override protected void Interactuar()` : Utilisa el metodo construir con un recurso

+ `@Override protected void construir( recurso )` : Dpende del recurso construye edificios y lo pociciona en el lugar del recurso , 
   Luego muere el aldeano

### Clase Jugador

Contiene la informacion del desarrollo del juego , actualmente solamente los recursos

+ `Constructor(int)` : Determina  Los recursos iniciales, estos son accedidos a trabes de getter

+ `modificarAcero(int)`: Modifica el  sumando el valor , en caso de ser menor a cero retorna false y no modifica

+ `modificarUranio(int)`: Modifica el  sumando el valor , en caso de ser menor a cero retorna false y no modifica

+ `modificarPlasma(int)`: Modifica el  sumando el valor , en caso de ser menor a cero retorna false y no modifica

+ `ponerRecurso(Recursos (Enum) ,Tupla 2D  )`: Ve el costo de poner un recurso determinado , modifcica , si existe suficiente recurso 
   crea una entidad recurso y la posiciona en el tablero y retorna true , en otro caso retorna false.



### Clase Recurso

Entidades , Tres subtipos de recursos , el jugador es capas de pocicionarlos en el mapa , un aldeano puede interactuar con estos para 
formar edificios de utilidad a la la civilisacione

 Uranio: recurso base aumentador (Aldeano + Uranio = Aumentdor)

 Acero: recurso base aumentador (Aldeano + Acero = Barraca)

 Plasma: recurso base aumentador (Aldeano + Plasma = Muralla)


## Setear la interfaz gráfica:

El juego usa libgdx para la interfaz gráfica. Para poder utilizarlo, seguir los siguientes pasos (mayor detalle [aquí](https://github.com/libgdx/libgdx/wiki/Setting-up-your-Development-Environment-%28Eclipse%2C-Intellij-IDEA%2C-NetBeans%29 "libgdx docs")).

1. Instalar Gradle como se explica [aquí](https://github.com/spring-projects/eclipse-integration-gradle/ "gradle").

2. Importar el proyecto, como se explica [aquí](https://github.com/libgdx/libgdx/wiki/Gradle-and-Eclipse "importing project").

3. Ejecutar el proyecto. Para eso hay que hacer click en el archivo `DesktopLauncher.java` del proyecto `LifeSpaceGame-desktop` y poner "Run as Java Application". Si se hizo todo correctamente se debiera abrir una ventana con la interfaz del juego.

Si se hizo todo bien, debería verse algo como la siguiente imagen:
![screen interfaz](https://bytebucket.org/regn/life-space/raw/b4189c06ba7259c4568d320e1cd196d842e4cfe4/ejemplo.png)

Se puede mover la cámara de la interfaz con las flechas.

## Tests (DEPRECATED)
En el main ejecutamos un test. Para crear todo lo necesario para tener una interfaz "jugable", hacemos lo siguiente:

1. Crear un tablero.

2. Crear una entidad, pasándole el tablero.

3. Setear la posición inicial de la entidad usando `posicionarInicialmente`.

4. Agregar la entidad al tablero usando `tablero.agregarEntidad`.

5. Llamar al método `start` de la entidad para que empieze a hacer su trabajo.



