package Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import Core.Celula;
import Core.Tablero;
import Core.TableroBuilder;
import Core.Tuple2D;
import Core.Utilidad;
import Entidades.Aldeano;
import Entidades.Entidad;
import Entidades.MarinoEspacial;
import Entidades.Zergling;

	//#########################################
	// ##################TESTS##################
	// #########################################
	public class JUnitTest{
		@Test
		public void testAssertMuerteUnidades(){
			Tablero tablero = TableroBuilder.build(3, 100, 100, false);
			List<Entidad> entidadesMatadas = new ArrayList<Entidad>();
			while(tablero.entidades.isEmpty() == false){
				Entidad currEntidad = tablero.entidades.get(0);
				currEntidad.matar();
				entidadesMatadas.add(currEntidad);
			}
			
			for (int x = 0; x < tablero.ancho; x++) {
				for (int y = 0; y < tablero.alto; y++) {
					org.junit.Assert.assertNull(tablero.getCelulaEnPosicion(x, y));
				}
			}
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			for(Entidad e : entidadesMatadas){
				org.junit.Assert.assertEquals(e.threadVivo(), false);
			}
		}
		
		@Test
		public void assertTableroInfinito() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException{
			Tablero tablero = new Tablero(64, 64);
	
			List<Entidad> entidadesCreadas = new ArrayList<Entidad>();
			for (int i = 0; i < tablero.alto*3; i++) {
				int x = Utilidad.random.nextInt();
				int y = Utilidad.random.nextInt();
				x = Math.abs(x);
				y = Math.abs(y);
				Entidad e = new Zergling("z", tablero);
				e.tiempoAccion = 10;
				e.posicionarInicialmente(new Tuple2D<Integer,Integer>(x,y));
				tablero.agregarEntidad(e);
				entidadesCreadas.add(e);	
			}
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Field celulasField = Tablero.class.getDeclaredField("celulas");
			celulasField.setAccessible(true);
			Field entidadesField = Tablero.class.getDeclaredField("entidadesPendientes");
			entidadesField.setAccessible(true);
			
			for(Entidad e : entidadesCreadas){
				boolean contains = tablero.entidades.contains(e);
				
				if(contains){ //Si se pudo agregar la entidad, debería tener una posicion en el tablero
					Celula[][] celulas = null;
					celulas = (Celula[][]) celulasField.get(tablero);
					Celula cellToTest = e.celulas.get(0);
					org.junit.Assert.assertEquals(e.celulas.get(0),  celulas[cellToTest.pos.x][cellToTest.pos.y]);
				}
				else{ //Si no se pudo agregar, debería estar en la lista de pendientes
					List<Entidad> entidadesPendientes = (List<Entidad>)entidadesField.get(tablero); 
					org.junit.Assert.assertTrue(entidadesPendientes.contains(e));
				}
			}
		}
		
		@Test
		public void assertEntidadesPendientesSonIngresadas() throws InterruptedException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
			Tablero tablero = new Tablero(64,64);
			int n = 5;
			for(int i = 0; i < n; i++){ //Ingresamos n entidades en la misma posicion
				Entidad aldeano = new Aldeano("me", tablero);
				aldeano.posicionarInicialmente(new Tuple2D<Integer,Integer>(0,0));
				aldeano.tiempoAccion = 10;
				tablero.agregarEntidad(aldeano);
			}
			//Esperamos un tiempo para que se abra espacio para que ingresen todas las entidades
			Thread.sleep(5000);
			Field entidadesField = Tablero.class.getDeclaredField("entidadesPendientes");
			entidadesField.setAccessible(true);
			List<Entidad> entidadesPendientes = (List<Entidad>)entidadesField.get(tablero); 

			org.junit.Assert.assertTrue(entidadesPendientes.isEmpty());
			org.junit.Assert.assertTrue(tablero.entidades.size() == n);
		}
	}