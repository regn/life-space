import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import Core.Jugador;
import Core.Tablero;
import Core.Tuple2D;
import Entidades.Aumentador;
import Entidades.MarinoEspacial;
import Entidades.Zergling;
import Entidades.Trampa;
import Entidades.Barraca;
import Entidades.CentroUrbano;
import Entidades.Muro;
import Entidades.Aldeano;
import Entidades.Acero;
import Entidades.Uranio;
import Entidades.Plasma;


public class Program {


	
	public static void main(String[] args) {
		
		
		// TODO: Crear un programa b�sico que testea el core.
		// Necesitamos esto para recibir input
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// Creamos un tablero
		Tablero tablero = new Tablero(15, 15);
		//Creamos un zergling
		

		Aumentador a = new Aumentador("A", tablero);
		//Lo ponemos en la posici�n inicial
		a.posicionarInicialmente(new Tuple2D<Integer, Integer>(5,3));
		tablero.agregarEntidad(a);
		a.start();
		
		Zergling e = new Zergling("z", tablero);
		//Lo ponemos en la posici�n inicial
		e.posicionarInicialmente(new Tuple2D<Integer, Integer>(1,1));
		//Lo agregamos al tablero
		tablero.agregarEntidad(e);
		
		System.out.println("Iniciando test 1: Instancia una entidad y la pone en el mapa. Presiona Enter para volver a imprimir la consola.");
		try {
			br.readLine();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int iterationCount = 0;
		while(iterationCount < 4){
			iterationCount++;
			//Imprimimos el tablero infinitamente.
			tablero.printEnConsola();
			System.out.println();
			System.out.println("Presiona Enter para imprimir el tablero. " + iterationCount + " de 4 Enters para siguiente test.");
			
			try {
				br.readLine();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		iterationCount = 0;
		System.out.println("Agregando un terr�cola a la grilla...");
		System.out.println("El zergling debiera comenzar a moverse hacia el terr�cola y asesinarlo.");
		try {
			br.readLine();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		MarinoEspacial me = new MarinoEspacial("m", tablero);
		me.posicionarInicialmente(new Tuple2D<Integer, Integer>(7,7));

		tablero.agregarEntidad(me);
		MarinoEspacial m = new MarinoEspacial("m", tablero);
		me.posicionarInicialmente(new Tuple2D<Integer, Integer>(10,10));
		tablero.agregarEntidad(m);


		e.start();
		while(iterationCount < 10){
			iterationCount++;
			//Imprimimos el tablero infinitamente.
			tablero.printEnConsola();
			System.out.println();
			System.out.println("Vida del marino: " + me.puntosVida);
			System.out.println("Presiona Enter para imprimir el tablero. " + iterationCount + " de 10 Enters para siguiente test.");
			try {
				br.readLine();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
	}

}
