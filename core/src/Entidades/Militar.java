package Entidades;


import Core.Tablero;

public abstract class Militar extends Entidad {
	
	protected double radioVision;
	protected double radioAtaque;
	protected int 	 ataque;

	public Militar(String nombre, Tablero tablero) {
		super(nombre, tablero);
		radioVision = 0;
		radioAtaque = 0;
		ataque = 0;
	}
	 
	
}
