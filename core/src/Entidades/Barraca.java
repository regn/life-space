package Entidades;

import java.util.*;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Tablero.facciones;


/*  FORMA PROVISIONAL BARRACAS
 *        OOO
 *       O O O
 *        OOO 
 * 
 */


public class Barraca extends Edificio {
	
	 private int posx;
	 private int posy;

	public Barraca(String nombre, Tablero tablero, int x , int y) {
		super(nombre, tablero);
		n_guardar="BA";
		faccion = facciones.Terricola;
		puntosVida = 30;
		ticksParaInteractuar= 17;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,1), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,2), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,3), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,0), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,2), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,4), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,2), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,3), this, Celula.colores.azul));
		
		posx=x;
		posy=y;
		
	}
		
	
	@Override
	protected void interactuar(){
		//creacion de marinos
		super.interactuar();
		if(cooldownInteractuar > 0)
			return;
		MarinoEspacial marino = new MarinoEspacial("me",tablero);
		marino.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx+3,posy));
		tablero.agregarEntidad(marino);
	}

}
