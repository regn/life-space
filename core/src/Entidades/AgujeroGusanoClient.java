package Entidades;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.badlogic.gdx.graphics.Color;


import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Utilidad;
import Core.Celula.colores;

public class AgujeroGusanoClient extends Edificio {
	
	protected Socket socket;
	private boolean conectado = false;
	private int radioAccion = 6;
	private boolean inicioServer = false;
	private final int GRUPO = 5;
	private static Map<Integer, String> gruposAIp; 
	private static Map<Integer, Integer> gruposAPuerto; 
	/* FORMA PROVISIONAL AGUJERO GUSANO
	 *        
	 *       OOO   
	 *      OOOOO
	 *       OOO
	 *         
	 */
	public AgujeroGusanoClient(String nombre, Tablero tablero) {
		super(nombre, tablero);
		if (gruposAIp == null){
			gruposAIp = new HashMap<Integer, String>();
			gruposAPuerto = new HashMap<Integer, Integer>();
			gruposAIp.put(1, "146.155.115.201");
			gruposAIp.put(2, "146.155.115.202");
			gruposAIp.put(3, "146.155.115.203");
			gruposAIp.put(4, "146.155.115.204");
			gruposAIp.put(5, "146.155.115.205");
			gruposAIp.put(6, "146.155.115.206");
			gruposAIp.put(7, "146.155.115.207");
			gruposAIp.put(8, "146.155.115.208");
			gruposAIp.put(9, "146.155.115.209");
			gruposAIp.put(10, "146.155.115.210");
			
			gruposAPuerto.put(1, 6969);
			gruposAPuerto.put(2, 1234);
			gruposAPuerto.put(3, 1234);
			gruposAPuerto.put(4, 8080);
			gruposAPuerto.put(5, 1769);
			gruposAPuerto.put(6, 1234);
			gruposAPuerto.put(7, 2222);
			gruposAPuerto.put(8, 1234);
			gruposAPuerto.put(9, 1234);
			gruposAPuerto.put(10, 1234);
		}
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,1), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,2), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,3), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,0), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this, Celula.colores.negro));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,2), this, Celula.colores.cyan));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,3), this, Celula.colores.negro));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,4), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(3,1), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(3,2), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(3,3), this, Celula.colores.magenta));
	}

	@Override
	protected void interactuar() {
		super.interactuar();
		if(inicioServer == false){
			try {
				socket = new Socket(gruposAIp.get(GRUPO), gruposAPuerto.get(GRUPO));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        try {
	        	socket.setSoTimeout(100000);
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        inicioServer = true;
	        conectado = true;
	        return;
		}
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioAccion);
		if(conectado == false){
			
		}
		
		if(conectado == true)
		{
			System.out.println("mandando msjes");
			double menorRadio = radioAccion;
			Entidad entAAbsorver = null;
			for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){
				Entidad e = i.next();
				double distanciaHaciaEntidad = Utilidad.distance(getCentroMasa(), e.getCentroMasa(), tablero);
				if(distanciaHaciaEntidad <= menorRadio  && e != this)
				{
					menorRadio = distanciaHaciaEntidad;
					entAAbsorver = e;
				}
			}
			if(entAAbsorver != null){
				System.out.println("enviando entidad");
				try {
					try {
						actualizarXmlEntidad(entAAbsorver);
					} catch (ParserConfigurationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (SAXException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					entAAbsorver.matar();
			         InputStream inFromServer = socket.getInputStream();
					DataInputStream in = new DataInputStream(inFromServer);
					String xml = in.readUTF();
					if(xml != ""){
						// Generar entidad
						try {
							Entidad e = generarEntidad(xml);
							tablero.agregarEntidad(e);
						} catch (ParserConfigurationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SAXException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					System.out.println("Entidad Recibida!");
					
					 
					OutputStream outToServer = socket.getOutputStream();
			         DataOutputStream out =
			                       new DataOutputStream(outToServer);
		            out.writeUTF(entAAbsorver.xmlRepresentation);
		            //System.out.println("Entidad enviada!: " + entAAbsorver.xmlRepresentation );
		            //out.writeUTF("Daniel: hola");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	           
			}
			 
	           
		}
	}
	
	private void actualizarXmlEntidad(Entidad e) throws ParserConfigurationException, SAXException, IOException {
		if(e.xmlRepresentation == "")
			e.xmlRepresentation = e.getFullXml();
		else{ 
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		    InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(e.xmlRepresentation));
			    
		    Document doc = db.parse(is);
		   
		    DocumentBuilder db2 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		    InputSource is2 = new InputSource();
		    is2.setCharacterStream(new StringReader(e.getXMLCommon()));
			    
		    Document doc2 = db2.parse(is2);
		    
		    DocumentBuilder db3 = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		    InputSource is3 = new InputSource();
		    is3.setCharacterStream(new StringReader(e.getXMLWorldSpecific()));
			    
		    Document doc3 = db3.parse(is3);
		    
		    Node newCommon = doc2.getElementsByTagName("Common").item(0);
		    
		    Node common = doc.getElementsByTagName("Common").item(0);
		    NodeList values = common.getChildNodes();
		    NodeList newValues = newCommon.getChildNodes();
		    for(int i = 0; i <  values.getLength() - 1; i++){
		    	Element curr = (Element)values.item(i);
		    	Element newCurr = (Element)newValues.item(i);
		    	curr.setNodeValue(getCharacterDataFromElement(newCurr));
		    }

		    
		    /*
		    Node newWorldSpecific = doc3.getElementsByTagName("World").item(0);
		    
		    Element worldSpecific = (Element) doc.getElementsByTagName("WorldSpecific").item(0);
		    
		    worldSpecific.appendChild(newWorldSpecific);
		    */
		    		
		    StringWriter outText = new StringWriter();
		    StreamResult sr = new StreamResult(outText);
		    Properties oprops = new Properties();
		    oprops.put(OutputKeys.METHOD, "html");
		    oprops.put("indent-amount", "4");
		    TransformerFactory tf = TransformerFactory.newInstance();
		    Transformer t = null;
		    try{
			    t = tf.newTransformer();
			    t.setOutputProperties(oprops);
			    t.transform(new DOMSource(doc),sr);
		    } catch(Exception ex){
		    	ex.printStackTrace();
		    }
		    e.xmlRepresentation = outText.toString();
		    
		  
		    	    		  
		}
	}
	
	private Entidad generarEntidad(String xml) throws ParserConfigurationException, SAXException, IOException{
		Entidad result = null; 
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	    InputSource is = new InputSource();
	    is.setCharacterStream(new StringReader(xml));
		    
	    Document doc = db.parse(is);
	    NodeList nodes = doc.getElementsByTagName("Common");
	    
	    for (int i = 0; i < nodes.getLength(); i++) {
	      Element element = (Element) nodes.item(i);

	      NodeList children = element.getChildNodes();
	      int posX = Integer.parseInt(getCharacterDataFromElement((Element)children.item(0)));
	      int posY = Integer.parseInt(getCharacterDataFromElement((Element)children.item(1)));
	      int width = Integer.parseInt(getCharacterDataFromElement((Element)children.item(2)));
	      int height = Integer.parseInt(getCharacterDataFromElement((Element)children.item(3)));
	      String originalGroupId= (getCharacterDataFromElement((Element)children.item(4)));
	      
	      result = generarEntidadRandom();
	      if(originalGroupId == Entidad.GROUP_ID){
	    	  // Obtener los valores de la entidad original
	    	 
	    	  NodeList worlds = doc.getElementsByTagName("World");
	    	  for(int j = 0; j < worlds.getLength(); j++){
	    		  Element world = (Element)worlds.item(j);
	    		  String id = world.getAttribute("id");
	    		  if(id == Entidad.GROUP_ID){
	    			  Element lifeForm = (Element) world.getChildNodes().item(0);
	    			  String type = lifeForm.getAttribute("type");
	    			  Element vida = (Element)lifeForm.getChildNodes().item(0);
	    			  if(type == "MarinoEspacial")
	    				  result = new MarinoEspacial("m-migrado", tablero);
	    			  else if(type == "TanqueEspacial")
	    				  result = new TanqueEspacial("t-Migrado", tablero);
	    			  else if(type == "Aldeano")
	    				  result = new Aldeano("a-Migrado", tablero);
	    			  else
	    				  result = new Zergling("z-Migrado", tablero);
	    			  result.puntosVida = Integer.parseInt(getCharacterDataFromElement(vida));
	    			  
	    		  }
	    	  }
	      }
	      for(Celula c : result.celulas){
	    	  if (c.pos.x == 1 && c.pos.y == 0)
	    		  c.setColor(colores.verde);
	      }
	      result.posicionarInicialmente(new Tuple2D<Integer, Integer>(posX, posY));
	      
	    }	

	    return result;
	}
	
	 public static String getCharacterDataFromElement(Element e) {
		    Node child = e.getFirstChild();
		    if (child instanceof CharacterData) {
		      CharacterData cd = (CharacterData) child;
		      return cd.getData();
		    }
		    return "";
		  }
	 
	 private Entidad generarEntidadRandom(){
		 int random = (int) (Utilidad.random.nextFloat()*4);
		 if(random == 0)
			 return new Aldeano("al-random", tablero);
		 else if(random == 1)
			 return new Zergling("z-random", tablero);
		 else if(random == 2)
			 return new MarinoEspacial("me-random", tablero);
		 else if(random == 3)
			 return new TanqueEspacial("te-random", tablero);
		 else
			 return new Aldeano("al-random", tablero);
		 
		 
	 }

}
