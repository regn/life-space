package Entidades;

import Core.Tablero;
import Core.Celula;
import Core.Tuple2D;
import Core.Tablero.facciones;

public class Acero extends Recurso {

	public Acero(String nombre, Tablero tablero) {
		super(nombre, tablero);
		
		n_guardar ="AC";
		faccion = facciones.Neutral;	
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this, Celula.colores.gris_claro));
		tipoRecurso = Recursos.Acero;
	}
	
	

}
