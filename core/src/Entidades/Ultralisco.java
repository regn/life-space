package Entidades;

import java.util.Iterator;
import java.util.List;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Tablero.facciones;

public class Ultralisco extends Zergling{
	
	public Ultralisco(String nombre, Tablero tablero) {
		super(nombre, tablero);
		n_guardar="UL";
		radioVision = 20;
		radioAtaque = 8;
		puntosVida  = 15;
		ataque		= 3;
		faccion		= facciones.Zerg;
		
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this, Celula.colores.verde));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,0), this, Celula.colores.gris_claro));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,1), this, Celula.colores.gris_claro));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,1), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this, Celula.colores.gris_claro));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,2), this, Celula.colores.gris_claro));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,2), this, Celula.colores.verde));
	}
	
	
	@Override
	protected void interactuar() {
		super.interactuar();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioAtaque);
		/*
		 * Hasta ahora atacamos a la primera entidad terr�cola en rango de ataque.
		 * TODO: Mejorar la inteligencia del zergling.
		 */
		if(cooldownInteractuar > 0)
			return;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){ // foreach en Java
			Entidad e = i.next();
			if(e.faccion == Tablero.facciones.Terricola)
			{
				e.atacar(this);
			}
		}
	}

	
}
