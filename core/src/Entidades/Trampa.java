package Entidades;

import java.util.Iterator;
import java.util.List;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Utilidad;
import Core.Tablero.facciones;


public class Trampa extends Militar {

    /**
	 * @uml.property  name="ataco"
	 */
    boolean ataco; //necesario porque debe hacer daño a todos los enemigos que encontro en su radio de ataque

	public Trampa(String nombre, Tablero tablero) {
		super(nombre, tablero);
		n_guardar="TR";
		radioVision = 1;
		radioAtaque = 8;
		puntosVida  = 100;  //es muy dificil de matar, si muere es porque exploto, haciendo daño a alguien
		ataque		= 20;
		faccion		= facciones.Terricola;
		ataco = false;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this, Celula.colores.naranjo)); // ocupa una celula
	}

		@Override
	protected void interactuar() {
		super.interactuar();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioVision);
		List<Entidad> entidadesafectadas= tablero.getEntidadesCercanas(getCentroMasa(), radioAtaque);

		if(cooldownInteractuar > 0)
			return;
		boolean atacar= false;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){ // foreach en Java
			Entidad e = i.next();
			
			if(e.faccion == Tablero.facciones.Zerg)
			{
				atacar = true;
			}			
		}
		
		if(atacar==true){
		for (Iterator<Entidad> i = entidadesafectadas.iterator(); i.hasNext();){
			Entidad e = i.next();
			e.atacar(this);
			ataco = true;
		}
		}
		
		if(ataco)
			matar();



	}
	
		

}