package Entidades;

import java.util.Iterator;
import java.util.List;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Utilidad;
import Core.Tablero.facciones;

public class RobotDeBatalla extends Militar{
 
	 int cooldownbomba;
	 int tiempo_bomba=20;
	
	public RobotDeBatalla( String nombre, Tablero tablero) {
		
		super(nombre, tablero);
		n_guardar="RB";
		cooldownbomba =20;
		n_guardar="RO";
		radioVision = 40;
		radioAtaque = 12;
		puntosVida  = 50;
		ataque		= 10;
		faccion		= facciones.Terricola;

		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this, Celula.colores.negro));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,1), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,2), this, Celula.colores.negro));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,2), this, Celula.colores.negro));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,0), this, Celula.colores.negro));
		
	}
	
	@Override
	protected void interactuar() {
		super.interactuar();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioAtaque);
		/*
		 * Hasta ahora atacamos a la primera entidad terrÃ¢â‚¬â„¢cola en rango de ataque.
		 * TODO: Mejorar la inteligencia del marino.
		 */
		if(cooldownInteractuar > 0)
			return;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){ // foreach en Java
			Entidad e = i.next();
			if(e.faccion == Tablero.facciones.Zerg)
			{
				e.atacar(this);
				break;
			}
		}
	}
	
	
	
	@Override
	protected void mover() {
		super.mover();
		
		
	
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioVision);
		/*
		 * Hasta ahora nos movemos a la entidad terrÃ¢â‚¬â„¢cola mÃ¢â‚¬Â¡s cercana.
		 * TODO: Mejorar la inteligencia del zergling.
		 */
		if(cooldownMoverse > 0)
			return;
		
		cooldownbomba--;
		
		if(cooldownbomba < 0){
		
			
			Trampa t  = new Trampa("ta", tablero);
			t.posicionarInicialmente(this.getCentroMasa());
			int base_x = this.getCentroMasa().x;
			int base_y = this.getCentroMasa().y;
			int aux_x;
			int aux_y;
			int intentos = 0;
			
			while(tablero.colisiona(t)){
				aux_x = base_x;
				aux_y = base_y;
				t  = new Trampa("ta", tablero);
				t.posicionarInicialmente(new Tuple2D<Integer,Integer>(aux_x+Utilidad.RandomInt(-2,2),aux_y+Utilidad.RandomInt(-2,2)));
				intentos++;
				if(intentos>100)
					break;
			}
			
			tablero.agregarEntidad(t);
			cooldownbomba=20;
			
		}
		
		Entidad entAPerseguir = null;
		double menorRadio = radioVision;
		
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){
			Entidad e = i.next();
			double distanciaHaciaEntidad = Utilidad.distance(getCentroMasa(), e.getCentroMasa(), tablero);
			if(e.faccion == Tablero.facciones.Zerg 
					&& distanciaHaciaEntidad <= menorRadio )
			{
				menorRadio = distanciaHaciaEntidad;
				entAPerseguir = e;
			}
		}
		// Si hay una entidad a perseguir en el rango de visiÃ¢â‚¬â€�n del alien, estarÃ¢â‚¬Â¡ almacenada en entAPerseguir
		if(entAPerseguir != null){
			moverHacia(entAPerseguir.getCentroMasa());
		}
		else
			moverRandom();
		
	}
	
	
	
}
