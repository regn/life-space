package Entidades;

import Core.Tablero;
import Core.Tablero.facciones;

public class Recurso extends Entidad {
   
	protected Recursos tipoRecurso;
	
	public Recurso(String nombre, Tablero tablero) {
		super(nombre, tablero);
		this.faccion = facciones.Neutral;
	}
	public Recursos get_tipoRecurso(){
		return tipoRecurso;
	}
}
