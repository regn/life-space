package Entidades;

import java.util.Iterator;
import java.util.List;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Utilidad;
import Core.Tablero.facciones;

public class Aldeano extends Trabajador {
    
	
	private boolean construyendo = false;
	
	public Aldeano(String nombre, Tablero tablero) {
		super(nombre, tablero);
		n_guardar ="AL";
		this.radioVision=50;
		this.radioInteraccion = 2;
		this.cooldownInteractuar = 20;
		puntosVida  = 15;
		faccion		= facciones.Terricola;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this, Celula.colores.amarillo));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,0), this, Celula.colores.amarillo));
		
	}
	

	@Override
	protected void mover() {
		/**
		 * Busca el recurso mas cercano dentro de su vision y se mueve hacia el
		 * */
		super.mover();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(),this.radioVision);
		if(cooldownMoverse > 0)
			return;
		
		Entidad entAPerseguir = null;
		double menorRadio = this.radioVision;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){
			Entidad e = i.next();
			double distanciaHaciaEntidad = Utilidad.distance(getCentroMasa(), e.getCentroMasa(), tablero);
			if(e instanceof Recurso && 
				distanciaHaciaEntidad <= menorRadio )
			{
				menorRadio = distanciaHaciaEntidad;
				entAPerseguir = e;
			}
		}
		// Si hay una entidad a perseguir en el rango de visi�n del aldeano, estar� almacenada en entAPerseguir
		if(entAPerseguir != null){
			moverHacia(entAPerseguir.getCentroMasa());
		}
		else
			moverRandom();
		
	}
	
	
	@Override
	protected void interactuar() {
		
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioInteraccion);
		construyendo = false;
		Recurso recursoAUtilizar = null;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){ // foreach en Java
			Entidad e = i.next();
			if(e.faccion == Tablero.facciones.Neutral && e instanceof Recurso)
			{
				construyendo = true;
				recursoAUtilizar = (Recurso)e;
				for(Celula c : this.celulas){
					c.setColor(Celula.colores.rosado);
				}
				break;
			}
		}
		if(construyendo == true){
			super.interactuar(); // ahora comienza a descontar el cooldown de interactuar
			
		}
		if(cooldownInteractuar > 0)
			return;
		
		construir(recursoAUtilizar);
	}
    
	
	protected void construir(Recurso r) {
		/**
		 * dependiendo del recurso que edificio construye , luego destruye el aldeano
		 * se construye un edificio en donde estaba el recurso
		 * */
		Edificio b;
		if(r.get_tipoRecurso() ==Recursos.Acero ){
			 b  = new Barraca("B", tablero,r.getCentroMasa().x,r.getCentroMasa().y);
		}else if(r.get_tipoRecurso() ==Recursos.Uranio) {
		     b = new Aumentador ("A", tablero);
		}else if(r.get_tipoRecurso() ==Recursos.Plasma) {
			 b  = new Muro("M", tablero);
		}else{b =null;}
		r.matar();
		Tuple2D< Integer, Integer> pocicion = r.getCentroMasa();
		b.posicionarInicialmente(pocicion);
		tablero.agregarEntidad(b);
		b.start();
		this.matar();
	}

}
