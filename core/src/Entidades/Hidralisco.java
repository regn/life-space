package Entidades;

import java.util.Iterator;
import java.util.List;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Utilidad;
import Core.Tablero.facciones;

public class Hidralisco extends Zergling{
	

	public Hidralisco(String nombre, Tablero tablero) {
		super(nombre, tablero);
		n_guardar="HI";
		radioVision = 20;
		radioAtaque = 4;
		puntosVida  = 8;
		ataque		= 3;
		faccion		= facciones.Zerg;
		
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,0), this, Celula.colores.cyan));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,1), this, Celula.colores.cyan));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,1), this, Celula.colores.rosado));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this, Celula.colores.cyan));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,2), this, Celula.colores.cyan));
	}
	

	
}
