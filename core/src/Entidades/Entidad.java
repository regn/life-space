package Entidades;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import Core.Celula;
import Core.Tablero.facciones;
import Core.Tuple2D;
import Core.Tablero;
import Core.Utilidad;
import LifeSpaceGame.GUI;
import PathFinding.Path;
import PathFinding.PathFinder;
import PathFinding.AStarPathFinder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public abstract class Entidad implements Runnable {
	/** tiempoAccion:double
	 *  Indica el tiempo que debe transcurrir para que la entidad realice una acci�n en milisegundos
	 */
	public long tiempoAccion = 500;
	public Tuple2D<Integer, Integer > referencia;
	private Path currentPath = null;
	private int currPathStep = 0;
	private boolean paused = false;
	
	public String n_guardar;
	protected boolean vivo;
	protected boolean started;
	public List<Celula> celulas;
	protected Tablero tablero;
	public int puntosVida;
	public static final String GROUP_ID = "LifeSpace";
	public Tablero.facciones faccion;
	public String id;
	protected int ticksParaMoverse;
	protected int ticksParaInteractuar;
	protected int cooldownInteractuar;
	protected int cooldownMoverse;
	public int cooldownAtacado = 0;
	public String xmlRepresentation = "";
	private Thread thread = null;
	private PathFinder pathFinder;
	public enum Recursos  {Acero, Plasma, Uranio }
	
	public Entidad(String nombre, Tablero tablero){
		referencia = new Tuple2D<Integer, Integer>(0, 0);
		id = nombre;
		faccion = facciones.Neutral;
		vivo = true;
		started = false;
		celulas = new ArrayList<Celula>();
		this.tablero = tablero;
		this.pathFinder = new AStarPathFinder(tablero, 500, false);
	}
	
	public void start(){
		started = true;
		if(thread == null){
			thread = new Thread(this, id);
			thread.start();
		}
	}
	
	public void detener(){
		started = false;
		thread.stop();
	}
	
	public void detenerResumir(){
		paused = !paused;
	}
	
	public boolean estaVivo(){
		return vivo;
	}
	
	public boolean threadVivo(){
		return thread.isAlive();
	}
	
	public void matar(){
		vivo = false;
		tablero.eliminarEntidad(this);
		if(this.n_guardar=="ZE"){
			tablero.get_jugador().modificarPuntaje(1);
		}
		
		
	}
	/**
	 * M�todo interactuar. Debe sobrescribirse con el comportamiento de la entidad concreta.
	 * Actualmente hace la l�gica del cooldown
	 */
	protected void interactuar(){
		cooldownInteractuar--;
		if(cooldownInteractuar < 0)
			cooldownInteractuar = ticksParaInteractuar;
		
	}

	
	/**
	 * M�todo mover. Debe sobrescribirse con el comportamiento de la entidad conctreta. 
	 * Actualmente hace la l�gica del cooldown
	 */
	protected void mover(){
		cooldownMoverse--;
		if(cooldownMoverse < 0)
			cooldownMoverse = ticksParaMoverse;
	}
	
	/**
	 * Metodo que se llama al crear el Thread. Este m�todo corre en un thread aparte.
	 * No deber�a ser necesario sobrescribir este m�todo.
	 */
	public void run(){
		// Iteramos mientras est� vivo
		while(vivo){
			
			// Si no ha sido inicializado, esperamos
			if(started == false)
				continue;
			
			// Esperamos a que est� listo para actuar
			try {
				Thread.sleep(tiempoAccion);
				if(paused)
					continue;
			} catch (InterruptedException e) {
				// TODO: Manejar excepci�n
			}
			
			// Movemos e interactuamos
			mover();
			interactuar();
		}
		
	}
	/**
	 * M�todo de utilidad que retorna la posicion del centro de la entidad.
	 * @return Tupla que contiene la posicion central de la entidad.
	 */
	public Tuple2D<Integer,Integer> getCentroMasa(){
		int numCelulasX = 0, numCelulasY = 0, totalPosX = 0, totalPosY = 0;
		for (Iterator<Celula> i = celulas.iterator(); i.hasNext();){ // foreach en Java
			Celula currCelula = i.next();
			numCelulasX++;
			numCelulasY++;
			totalPosX += currCelula.pos.x;
			totalPosY += currCelula.pos.y;
		}
		int resultX = (int)(totalPosX / numCelulasX);
		int resultY = (int)(totalPosY / numCelulasY);
		Tuple2D<Integer,Integer> result = new Tuple2D<Integer, Integer>(resultX, resultY);
		return result;
	}
	
	public void atacar(Militar atacante){
		// TODO: Validar que el atacante est� en el rango del atacado
		puntosVida -= atacante.ataque;
		cooldownAtacado = GUI.GuiTicksAtacar;
		if(puntosVida <= 0)
			matar();
	}
	
	protected void construir(Entidad e){
		
		
	}
	
	/**
	 * M�todo auxiliar para mover la entidad hacia la posici�n objetivo. No valida que no se haya movido este tick.
	 * @param destino
	 */
	protected void moverHacia(Tuple2D<Integer, Integer> destino){
		Tuple2D<Integer,Integer> currPos = getCentroMasa();
		if (currentPath == null){
			currentPath = pathFinder.findPath(null, currPos.x, currPos.y, destino.x, destino.y);
			currPathStep = 0;
		}
		if(currentPath == null)
			return;
		currPathStep++;
		if(currentPath.getLength() <= currPathStep){
			currentPath = null;
			currPathStep = 0;
			return;
		}
		Tuple2D<Integer, Integer> newPos = new Tuple2D<Integer, Integer>(currentPath.getX(currPathStep), currentPath.getY(currPathStep));
		
		Tuple2D<Integer, Integer> desp = new Tuple2D<Integer, Integer>(newPos.x - currPos.x, newPos.y - currPos.y);
		if(desp.x == 0 && desp.y == 0){
			//meta alcanzada
			currentPath = null;
			currPathStep = 0;
		}
		boolean noColisiono = false;
		if(Math.abs(desp.x) > Math.abs(desp.y)){
			// Debemos acortar la distancia horizontal.
			int despX = (int)Math.signum(desp.x);
			noColisiono = moverAux(despX, true);
		}
		else{
			int despY = (int)Math.signum(desp.y);
			noColisiono = moverAux(despY, false);
		}
		
		
	}
	
	private boolean moverAux(int ammount, boolean esMovHorizontal){
		
		tablero.eliminarPosicionEntidad(this);
		
		boolean colision = false;
		
		for (Iterator<Celula> i = celulas.iterator(); i.hasNext();){ // foreach en Java
			Celula currCelula = i.next();
			int posiblePosX = (currCelula.pos.x+ammount);
			if (posiblePosX < 0) posiblePosX += tablero.ancho;
			posiblePosX %= tablero.ancho;
			int posiblePosY = (currCelula.pos.y+ammount);
			if (posiblePosY < 0) posiblePosY += tablero.alto;
			posiblePosY %= tablero.alto;
			if(esMovHorizontal && tablero.getCelulaEnPosicion(posiblePosX, currCelula.pos.y) != null){
            	colision = true;
            	break;
            }
            else if (!esMovHorizontal && tablero.getCelulaEnPosicion(currCelula.pos.x, posiblePosY) != null){
            	colision = true;
            	break;
            }
			
		}
		if (!colision){
			for (Iterator<Celula> i = celulas.iterator(); i.hasNext();){ // foreach en Java
				Celula currCelula = i.next();
				
				if(esMovHorizontal){
					referencia.x+=ammount;
					currCelula.pos.x += ammount;
					if(currCelula.pos.x < 0){
						currCelula.pos.x += tablero.ancho; 
					}
					if(referencia.x < 0){
						referencia.x += tablero.ancho; 
					}
					
					referencia.x %= tablero.ancho;
					currCelula.pos.x %= tablero.ancho; // Asegurarse de que no se escape fuera del tablero
					
				}
				else {
					
					currCelula.pos.y += ammount;
					referencia.y+=ammount;
					if(currCelula.pos.y < 0){
						currCelula.pos.y += tablero.alto; 	 	
					}
					if(referencia.y < 0){
						referencia.y += tablero.alto; 	 	
					}
					
					currCelula.pos.y %= tablero.alto; // Asegurarse de que no se escape fuera del tablero		
					referencia.y %= tablero.alto;
				}
				
			}	
		}
		tablero.insertarPosicionEntidad(this);
		return !colision;
		
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return id;
	}
	
	public void posicionarInicialmente(Tuple2D<Integer, Integer> pos){
		referencia.x= pos.x;
		referencia.y=pos.y;
		for (Iterator<Celula> i = celulas.iterator(); i.hasNext();){ // foreach en Java
			Celula currCelula = i.next();
			currCelula.pos.x += pos.x;
			currCelula.pos.y += pos.y;
			// Tablero circular:
			currCelula.pos.x %= tablero.ancho;
			currCelula.pos.y %= tablero.alto;
			referencia.x%=tablero.ancho;
			referencia.y%=tablero.alto;
			
		}
	}
	
	public String getXMLCommon(){
		Tuple2D<Integer, Integer> centroMasa = getCentroMasa();
		int maxWidth = -1, maxHeight = -1, minWidth = 99999, minHeight = 99999;
		for(Celula c : celulas){
			if(c.pos.x > maxWidth)
				maxWidth = c.pos.x;
			if(c.pos.y > maxHeight)
				maxHeight = c.pos.y;
			if(c.pos.y < minHeight)
				minHeight = c.pos.y;
			if(c.pos.x < minWidth)
				minWidth = c.pos.x;
		}
		int width = maxWidth - minWidth;
		int height = maxHeight - minHeight;
		
	    String result = "<Common>"+
        "<PosX>"+ centroMasa.x +"</PosX>"+ 
        "<PosY>"+ centroMasa.y + "</PosY>"+ 
        "<Width>"+ width + "</Width>"+ 
        "<Height>"+ height + "</Height>"+ 
        "<OriginalGroupID>"+ GROUP_ID + "</OriginalGroupID>"+ 
    "</Common> ";
	    
	    return result;

	}
	
	public String getXMLWorldSpecific(){
		String type = "Zergling";
		if(this instanceof MarinoEspacial)
			type = "MarinoEspacial";
		if(this instanceof TanqueEspacial)
			type = "TanqueEspecial";
		if(this instanceof Aldeano)
			type = "Aldeano";
		
		String result = "<World id=\""+ GROUP_ID + "\">"+
					"<SpaceLifeForm type=\"" + type + "\">" +
						"<PuntosDeVida>" + this.puntosVida + "</PuntosDeVida>" +  
					"</SpaceLifeForm>" +
				"</World>";
        return result;
              
             
	}
	public String getFullXml(){
		String result = "<GameOfLife>" +
							getXMLCommon() +
							"<WorldSpecific>" +
								getXMLWorldSpecific()+
							"</WorldSpecific>"+
						"</GameOfLife>";
		return result;
	}
	
	protected void moverRandom(){
		float random = Utilidad.random.nextFloat();
		if(random < 0.25f)
			moverAux(1, true);
		else if(random < 0.5f)
			moverAux(1, false);
		else if(random < 0.75f)
			moverAux(-1, true);
		else
			moverAux(-1, false);
		
	}
}