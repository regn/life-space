package Entidades;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Tablero.facciones;

public class Uranio extends Recurso {

	public Uranio(String nombre, Tablero tablero) {
		super(nombre, tablero);
		n_guardar="UR";
		faccion = facciones.Neutral;	
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this, Celula.colores.verde));
		tipoRecurso = Recursos.Uranio;
	}
	
}
