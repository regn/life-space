package Entidades;

import Core.*;
import Core.Tablero.facciones;


//Edificio de donde salen los Zerglings

public class Madriguera extends Edificio{
	
	
	public Madriguera(String nombre, Tablero tablero){
		super(nombre, tablero);
		n_guardar ="MA";
		puntosVida = 1000; //muy resistente	
		faccion = facciones.Neutral;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,0), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,1), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,2), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,2), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,2), this, Celula.colores.magenta));		

	}
	


	
}
