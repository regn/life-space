package Entidades;

import java.util.*;
import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Tablero.facciones;

public class CentroUrbano extends Edificio {
	
	/* FORMA PROVISIONAL CENTRO URBANO
	 *        O
	 *       OOO   
	 *      OOOOO
	 *       OOO
	 *        O 
	 */
	
	private int posx;
	private int posy;

	public CentroUrbano(String nombre, Tablero tablero, int x , int y) {	
		super(nombre, tablero);
		n_guardar ="CE";
		puntosVida = 500; //muy resistente	
		ticksParaInteractuar = 15;
		faccion = facciones.Terricola;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,2), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,1), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,2), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,3), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,0), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,2), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,3), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,4), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(3,1), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(3,2), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(3,3), this, Celula.colores.azul));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(4,2), this, Celula.colores.azul));
		
		posx=x;
		posy=y;
	}	

	@Override
	protected void interactuar(){
		super.interactuar();
		
		//creacion de aldeanos		
		if(cooldownInteractuar > 0)
			return;
		Aldeano alde = new Aldeano("AL",tablero);
		alde.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx,posy-1));
		tablero.agregarEntidad(alde);
	}
	


}
