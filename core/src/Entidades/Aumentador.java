package Entidades;

import java.awt.Event;
import java.beans.EventHandler;

import Core.Celula;
import Core.Tablero;
import Core.Jugador;
import Core.Tuple2D;
import Core.Tablero.facciones;
public class Aumentador extends Edificio {

	private int produccion_acero;
	private int produccion_uranio;
	private int produccion_plasma;
	
	/*  FORMA PROVISIONAL Aumentador
	 *       OOO
	 *       OOO
	 *       OOO 
	 * 
	 */
	public Aumentador(String nombre, Tablero tablero) {
		super(nombre, tablero);
	    n_guardar="AU";
		puntosVida  = 100;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,0), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,1), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,1), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,2), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,2), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,2), this, Celula.colores.magenta));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,0), this, Celula.colores.magenta));
		this.ticksParaInteractuar=20;
	    faccion = facciones.Terricola;
		/**
		 * definimos produccion , 
		 * modificable si no es conveniente
		 * */
		this.produccion_acero=3;
	    this.produccion_uranio=2;
	    this.produccion_plasma=1;
		
	}
	@Override
	protected void mover() {
		// TODO Auto-generated method stub
		super.mover();
	}
	@Override
	protected void interactuar(){
		/**
		 * por cada interaccion que realiza modifica los recursos del jugador segun los niveles de 
		 * produccion estableciondos es el constructor
		 * */
		super.interactuar();
		if(cooldownInteractuar > 0)
			return;
		super.interactuar();
		Jugador jugador = this.tablero.get_jugador();
		jugador.modificarAcero(produccion_acero);
		jugador.modificarUranio(produccion_plasma);
		jugador.modificarPlasma(produccion_uranio);
		
	}


	
	

}
