package Entidades;

import java.util.Iterator;
import java.util.List;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Utilidad;
import Core.Tablero.facciones;

public class TanqueEspacial extends Militar {

	public TanqueEspacial(String nombre, Tablero tablero) {
		super(nombre, tablero);
		n_guardar="TA";
		radioVision = 20;
		radioAtaque = 4;
		puntosVida  = 30;
		ataque		= 6;
		faccion		= facciones.Terricola;

		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,0), this, Celula.colores.blanco));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,0), this, Celula.colores.blanco));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,1), this, Celula.colores.blanco));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,1), this, Celula.colores.blanco));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this, Celula.colores.blanco));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(3,1), this, Celula.colores.blanco));
		
	}	
	
	@Override
	protected void interactuar() {
		super.interactuar();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioAtaque);
		/*
		 * Hasta ahora atacamos a la primera entidad terrÃ¢â‚¬â„¢cola en rango de ataque.
		 * TODO: Mejorar la inteligencia del marino.
		 */
		if(cooldownInteractuar > 0)
			return;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){ // foreach en Java
			Entidad e = i.next();
			if(e.faccion == Tablero.facciones.Zerg)
			{
				e.atacar(this);
				break;
			}
			else if(e.faccion == Tablero.facciones.Neutral && e instanceof Recurso && ((Recurso)e).get_tipoRecurso() == Entidad.Recursos.Plasma ){
				e.matar();
				RobotDeBatalla t  = new RobotDeBatalla("ta", tablero);
				t.posicionarInicialmente(this.getCentroMasa());
				int base_x = this.getCentroMasa().x;
				int base_y = this.getCentroMasa().y;
				int aux_x;
				int aux_y;
				int intentos = 0;
				while(tablero.colisiona(t)){
					aux_x = base_x;
					aux_y = base_y;
					t  = new RobotDeBatalla("ta", tablero);
					t.posicionarInicialmente(new Tuple2D<Integer,Integer>(aux_x+Utilidad.RandomInt(-2,2),aux_y+Utilidad.RandomInt(-2,2)));
					intentos++;
					if(intentos>100)
						break;
				}
				tablero.agregarEntidad(t);	
				this.matar();
				break;
			}
		}
	}
	
	@Override
	protected void mover() {
		super.mover();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioVision);
		/*
		 * Hasta ahora nos movemos a la entidad terrÃ¢â‚¬â„¢cola mÃ¢â‚¬Â¡s cercana.
		 * TODO: Mejorar la inteligencia del zergling.
		 */
		if(cooldownMoverse > 0)
			return;
		
		Entidad entAPerseguir = null;
		double menorRadio = radioVision;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){
			Entidad e = i.next();
			double distanciaHaciaEntidad = Utilidad.distance(getCentroMasa(), e.getCentroMasa(), tablero);
			if(e.faccion == Tablero.facciones.Zerg ||
					(e.faccion == Tablero.facciones.Neutral && e instanceof Recurso && ((Recurso)e).get_tipoRecurso() == Entidad.Recursos.Plasma)
					&& distanciaHaciaEntidad <= menorRadio )
			{
				menorRadio = distanciaHaciaEntidad;
				entAPerseguir = e;
			}
		}
		// Si hay una entidad a perseguir en el rango de visiÃ¢â‚¬â€�n del alien, estarÃ¢â‚¬Â¡ almacenada en entAPerseguir
		if(entAPerseguir != null){
			moverHacia(entAPerseguir.getCentroMasa());
		}
		else
			moverRandom();
		
	}
	

}