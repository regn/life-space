package LifeSpaceGame;

import java.util.Iterator;
import java.util.Map;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

import Core.Celula;
import Core.Jugador;
import Core.TableroBuilder;
import Core.Tablero;
import Core.Tuple2D;
import Core.Celula.colores;
import Entidades.AgujeroGusano;
import Entidades.AgujeroGusanoClient;
import Entidades.Entidad;
import Entidades.Entidad.Recursos;

public class GUI extends ApplicationAdapter {
	SpriteBatch batch;
	SpriteBatch textRenderer;
	ShapeRenderer shapeRenderer;
	BitmapFont font;
	OrthographicCamera camera;
	boolean mousePressed = false;
	boolean spacePressed = false;
	boolean pPressed = false;
	boolean oPressed = false;
	boolean sPressed = false;
	boolean lPressed = false;
	boolean Pressed1 = false;
	boolean Pressed2 = false;
	boolean Pressed3 = false;
	boolean cPressed = false;
	int 	ticksZPressed = 0;
	int 	ticksXPressed = 0;
	int  dificultad = 1;
	
	boolean cargar =false;
	
	Rectangle terreno;
	Rectangle[] celulas;
	private int CAMERA_WIDTH = 800;
	private int CAMERA_HEIGHT = 480;
	public static final int GuiTicksAtacar = 20;
	private final int VEL_CAMARA = 400;
	private Recursos recursoSeleccionado = Recursos.Acero; 
	//Texture terrenoImage;
	Tablero tablero;
	Jugador jugador;
	ControladorOleadas oleadas;
	Timer time;
	private int LARGO_CELULA_EN_PX = 24;
	private Texture background;

	@Override
	public void create () {
		batch = new SpriteBatch();
		textRenderer = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, CAMERA_WIDTH, CAMERA_HEIGHT);
		font = new BitmapFont();
		background = new Texture(Gdx.files.internal("terrain.png"));
		// Crear un tablero 

		
		tablero = TableroBuilder.build(dificultad, 64,64 , cargar);
		
		
		camera.position.y = tablero.alto * LARGO_CELULA_EN_PX/2;
		camera.position.x = tablero.ancho* LARGO_CELULA_EN_PX/2;

		
		//Controlador Oleadas
		oleadas= new ControladorOleadas();
		ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                oleadas.Ingreso(tablero);
            }

        };
		time = new Timer(7000,action);

		// Setear sprites del juego 
		terreno = new Rectangle();
		terreno.width = LARGO_CELULA_EN_PX * tablero.ancho;
		terreno.height = LARGO_CELULA_EN_PX * tablero.alto;
		terreno.x = terreno.y = 0;
		//terrenoImage = new Texture(Gdx.files.internal("terrain.jpg"));
		
		// Iniciar jugador
		jugador = tablero.get_jugador();
		// Iniciar tablero
		time.start();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0.4f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		// Procesar input del usuario
		if(Gdx.input.isKeyPressed(Keys.LEFT)) camera.position.x -= VEL_CAMARA * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) camera.position.x += VEL_CAMARA * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Keys.UP)) camera.position.y += VEL_CAMARA * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Keys.DOWN)) camera.position.y -= VEL_CAMARA * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Keys.SPACE)) spacePressed = true;
		else if(spacePressed == true){
			spacePressed = false;
			recursoSeleccionado =Recursos.values()[(recursoSeleccionado.ordinal()+1)%Recursos.values().length];
		}
		if(Gdx.input.isKeyPressed(Keys.P)) pPressed = true;
		else if(pPressed){
			int posYTab = (int)((tablero.alto)*LARGO_CELULA_EN_PX - ( -Gdx.input.getY() + camera.position.y + CAMERA_HEIGHT/2))/LARGO_CELULA_EN_PX;
			int posXTab = (int)(Gdx.input.getX() + (camera.position.x - CAMERA_WIDTH/2) )/LARGO_CELULA_EN_PX;
			if(posYTab < 0) posYTab += tablero.alto;
			if(posXTab < 0) posXTab += tablero.ancho;
			posYTab %= tablero.alto;
			posXTab %= tablero.ancho;
			AgujeroGusano ag = new AgujeroGusano("agujero", tablero);
			ag.posicionarInicialmente(new Tuple2D<Integer, Integer>(posXTab, posYTab));
			tablero.agregarEntidad(ag);
			pPressed = false;
		}
		
		if(Gdx.input.isKeyPressed(Keys.O)) oPressed = true;
		else if(oPressed){
			int posYTab = (int)((tablero.alto)*LARGO_CELULA_EN_PX - ( -Gdx.input.getY() + camera.position.y + CAMERA_HEIGHT/2))/LARGO_CELULA_EN_PX;
			int posXTab = (int)(Gdx.input.getX() + (camera.position.x - CAMERA_WIDTH/2) )/LARGO_CELULA_EN_PX;
			if(posYTab < 0) posYTab += tablero.alto;
			if(posXTab < 0) posXTab += tablero.ancho;
			posYTab %= tablero.alto;
			posXTab %= tablero.ancho;
			AgujeroGusanoClient ag = new AgujeroGusanoClient("agujero", tablero);
			ag.posicionarInicialmente(new Tuple2D<Integer, Integer>(posXTab, posYTab));
			tablero.agregarEntidad(ag);
			oPressed = false;
		}
		if(Gdx.input.isKeyPressed(Keys.S)) sPressed = true;
		else if(sPressed){
			TableroBuilder.Guardar(tablero);
			sPressed=false;
		}
		if(Gdx.input.isKeyPressed(Keys.L)) lPressed = true;
		else if(lPressed){
		    this.dispose();
		    cargar=true;
		    this.create();
		    cargar=false;
			lPressed=false;
		}
		if(Gdx.input.isKeyPressed(Keys.F1)) Pressed1 = true;
		else if(Pressed1){
		    this.dispose();
		    dificultad=1;
		    this.create();
		    dificultad=1;
			Pressed1=false;
		}
		if(Gdx.input.isKeyPressed(Keys.F2)) Pressed2 = true;
		else if(Pressed2){
		    this.dispose();
		    dificultad=2;
		    this.create();
		    dificultad=1;
			Pressed2=false;
		}
		if(Gdx.input.isKeyPressed(Keys.F3)) Pressed3 = true;
		else if(Pressed3){
		    this.dispose();
		    dificultad=3;
		    this.create();
		    dificultad=1;
			Pressed3=false;
		}
		int tickForZoom = 5;
		if(Gdx.input.isKeyPressed(Keys.Z)){
			ticksZPressed++;
			if(ticksZPressed >= tickForZoom){
				ticksZPressed = 0;
				if(LARGO_CELULA_EN_PX < 100)
					LARGO_CELULA_EN_PX++;
			}
			
		}
		if(Gdx.input.isKeyPressed(Keys.X)){
			ticksXPressed++;
			if(ticksXPressed >= tickForZoom){
				ticksXPressed = 0;
				if(LARGO_CELULA_EN_PX > 5)
					LARGO_CELULA_EN_PX--;
			}
			
		}
		if(Gdx.input.isKeyPressed(Keys.C)) cPressed = true;
		else if(cPressed){
		    for(Entidad e : tablero.entidades)
		    	e.detenerResumir();
		    if(time.isRunning())
		    	time.stop();
		    else
		    	time.start();
		    cPressed = false;
		}
		
		//Resetear posicion de camara si se sale del tablero
		if(cameraOutOfBoundaries())
			resetCameraPosition();
		
		
		camera.update();
		
		// Mouse input:
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
			mousePressed = true;
		}
		else if(mousePressed == true){
			int posYTab = (int)((tablero.alto)*LARGO_CELULA_EN_PX - ( -Gdx.input.getY() + camera.position.y + CAMERA_HEIGHT/2))/LARGO_CELULA_EN_PX;
			int posXTab = (int)(Gdx.input.getX() + (camera.position.x - CAMERA_WIDTH/2) )/LARGO_CELULA_EN_PX;
			if(posYTab < 0) posYTab += tablero.alto;
			if(posXTab < 0) posXTab += tablero.ancho;
			posYTab %= tablero.alto;
			posXTab %= tablero.ancho;
			jugador.ponerRecurso(recursoSeleccionado, new Tuple2D<Integer,Integer>(posXTab, posYTab));
			
			mousePressed = false;
		}
		
		// Dibujar background
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		renderBackground();
		batch.end();
		// Dibujar entidades
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Filled);
		try {
			tablero.semaModEntidades.acquire();
			for(Iterator<Entidad> itEnt = tablero.entidades.iterator(); itEnt.hasNext();){
				Entidad currEntidad = itEnt.next();
				if(currEntidad.cooldownAtacado > 0){
					float redQty = (float) (0.5+(Math.sin(1-10*currEntidad.cooldownAtacado/GuiTicksAtacar))/4);
					shapeRenderer.setColor(redQty, 0.0f, 0.0f, 1.0f);
					currEntidad.cooldownAtacado--;
				}
				else
					currEntidad.cooldownAtacado = 0;
				for(Iterator<Celula> itCel = currEntidad.celulas.iterator(); itCel.hasNext();){
					Celula currCel = itCel.next();
					
					if(currEntidad.cooldownAtacado == 0){
						shapeRenderer.setColor(currCel.color);
					}
					renderCell(currCel);
				}
			}
			tablero.semaModEntidades.release();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		shapeRenderer.end();
		
		// Dibujar texto
		
		
		String uranio = "Uranio: " + jugador.getUranio();
		String acero = "Acero: " + jugador.getAcero();
		String plasma = "Plasma: " + jugador.getPlasma();
		 
		textRenderer.begin();
		if(recursoSeleccionado == Recursos.Uranio) font.setColor(0.7f, 1.0f, 0.1f, 1.0f);
		else font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		font.draw(textRenderer, uranio, 10, CAMERA_HEIGHT - 80);
		if(recursoSeleccionado == Recursos.Acero) font.setColor(0.7f, 1.0f, 0.1f, 1.0f);
		else font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		font.draw(textRenderer, acero, 10, CAMERA_HEIGHT - 100);
		if(recursoSeleccionado == Recursos.Plasma) font.setColor(0.7f, 1.0f, 0.1f, 1.0f);
		else font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		font.draw(textRenderer, plasma, 10, CAMERA_HEIGHT - 120);
		font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		font.draw(textRenderer, "Espacio para cambiar", 10, CAMERA_HEIGHT - 140);
		font.draw(textRenderer, "Oleada: " + oleadas.getNumeroOleada() , CAMERA_WIDTH - 80, CAMERA_HEIGHT - 80);
		font.draw(textRenderer, "Puntaje: " + jugador.getPuntaje() , CAMERA_WIDTH - 80 , CAMERA_HEIGHT - 20);
		
		textRenderer.end();
		
		
		
	}
	
	private void renderCell(Celula cell){
		int posXPx, posYPx;
		
		// Renderear en cuadrantes derechos
		posXPx = (cell.pos.x + tablero.ancho)*LARGO_CELULA_EN_PX;
		posYPx = (tablero.alto*2 - cell.pos.y - 1) * (LARGO_CELULA_EN_PX);
		shapeRenderer.rect(posXPx, posYPx, LARGO_CELULA_EN_PX, LARGO_CELULA_EN_PX);
		posYPx = (tablero.alto - cell.pos.y - 1) * (LARGO_CELULA_EN_PX);
		shapeRenderer.rect(posXPx, posYPx, LARGO_CELULA_EN_PX, LARGO_CELULA_EN_PX);
		posYPx = (-cell.pos.y - 1) * (LARGO_CELULA_EN_PX);
		shapeRenderer.rect(posXPx, posYPx, LARGO_CELULA_EN_PX, LARGO_CELULA_EN_PX);
		
		// Renderear en cuadrantes izquierdos
		posXPx = (cell.pos.x - tablero.ancho)*LARGO_CELULA_EN_PX;
		posYPx = (tablero.alto*2 - cell.pos.y - 1) * (LARGO_CELULA_EN_PX);
		shapeRenderer.rect(posXPx, posYPx, LARGO_CELULA_EN_PX, LARGO_CELULA_EN_PX);
		posYPx = (tablero.alto - cell.pos.y - 1) * (LARGO_CELULA_EN_PX);
		shapeRenderer.rect(posXPx, posYPx, LARGO_CELULA_EN_PX, LARGO_CELULA_EN_PX);
		posYPx = (-cell.pos.y - 1) * (LARGO_CELULA_EN_PX);
		shapeRenderer.rect(posXPx, posYPx, LARGO_CELULA_EN_PX, LARGO_CELULA_EN_PX);
		
		// Renderear en cuadrantes centrales
		posXPx = cell.pos.x * LARGO_CELULA_EN_PX;
		posYPx = (tablero.alto*2 - cell.pos.y - 1) * (LARGO_CELULA_EN_PX);
		shapeRenderer.rect(posXPx, posYPx, LARGO_CELULA_EN_PX, LARGO_CELULA_EN_PX);
		posYPx = (tablero.alto - cell.pos.y - 1) * (LARGO_CELULA_EN_PX);
		shapeRenderer.rect(posXPx, posYPx, LARGO_CELULA_EN_PX, LARGO_CELULA_EN_PX);
		posYPx = (-cell.pos.y - 1) * (LARGO_CELULA_EN_PX);
		shapeRenderer.rect(posXPx, posYPx, LARGO_CELULA_EN_PX, LARGO_CELULA_EN_PX);
	}
	
	private void renderBackground(){
		int repeats = (int)tablero.ancho*LARGO_CELULA_EN_PX*3/background.getWidth();
		for(int x = -tablero.ancho; x < tablero.ancho*2; x++){
			for(int y = -tablero.alto; y < tablero.alto*2; y++){
				batch.draw(background, x*background.getWidth(), y*background.getHeight());
			}
		}
	}
	
	private boolean cameraOutOfBoundaries(){
		boolean outOfX = false;
		boolean outOfY = false;
		
		if(camera.position.x + CAMERA_WIDTH/2 > tablero.ancho*LARGO_CELULA_EN_PX)
			outOfX = true;
		if(camera.position.x + CAMERA_WIDTH/2 < 0)
			outOfX = true;
		
		if(camera.position.y + CAMERA_HEIGHT/2 > tablero.alto*LARGO_CELULA_EN_PX)
			outOfY = true;
		if(camera.position.y + CAMERA_HEIGHT/2 < 0)
			outOfY = true;
		
		return outOfX || outOfY;
		
	}
	
	private void resetCameraPosition(){
		if(camera.position.x < 0)
			camera.position.x += tablero.ancho * LARGO_CELULA_EN_PX;
		else
			camera.position.x %= tablero.ancho * LARGO_CELULA_EN_PX;
		
		if(camera.position.y < 0)
			camera.position.y += tablero.alto * LARGO_CELULA_EN_PX;
		else
			camera.position.y %= tablero.alto * LARGO_CELULA_EN_PX;
		
	}
	
	@Override
	public void dispose() {
		batch.dispose();
		shapeRenderer.dispose();
	}
}