package LifeSpaceGame;

import Core.*;
import Entidades.*;

public class ControladorOleadas {

	//Por ahora solo ingresan por Madriguera cada 5 segundos!
	
	/* Clase controladora de las invasiones de Zerglings
	 * contra el Jugador, ingresan cada cierto tiempo al tablero
	 * destruyendo todo lo que est� en su camino
	 *Va aumentando el ingreso de Zerglings cada segundo que pasa
	 */
	
	private int aumentador=0;
	private int auhid=1;
	private int auult=1;
	private Zergling zer;
	private Hidralisco hid;
	private Ultralisco ult;
	
	public ControladorOleadas()
	{
		//Inicializo la primera oleada solo de 1 zergling
		aumentador=1;
	}
	
	public Tablero Ingreso (Tablero tablero)
	{
		//Agrego a todos los zerglings al tablero
		int x = tablero.getPosxma();
		int y = tablero.getPosyma();
		
		if(aumentador%10==0)
		{
			for(int i=0; i< auult;i++)
			{			
				ult = new Ultralisco("u", tablero);
			    ult.posicionarInicialmente(new Tuple2D<Integer, Integer>(x+3,y));		
				tablero.agregarEntidad(ult);
				y+=4;
			}
			auult++;
		}
		
		if(aumentador%5==0)
		{
			for(int i=0; i< auhid;i++)
			{			
				hid = new Hidralisco("h", tablero);
			    hid.posicionarInicialmente(new Tuple2D<Integer, Integer>(x+3,y));		
				tablero.agregarEntidad(hid);
				y+=4;
			}
			auhid++;
		}

		for(int i=0; i< aumentador;i++)
		{			
			zer = new Zergling("z", tablero);
		    //Lo ponemos en la posicion inicial
		    zer.posicionarInicialmente(new Tuple2D<Integer, Integer>(x+3,y));		
			tablero.agregarEntidad(zer);
			y+=4;
			
		}
		
		aumentador++;
		return tablero;
	}
	
	
	
	public int getNumeroOleada(){
		return aumentador;
	}
	
	

}
