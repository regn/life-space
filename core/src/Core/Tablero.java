package Core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

//import org.junit.Test;

import Core.Tuple2D;
import Entidades.Entidad;
import Entidades.TanqueEspacial;
import Entidades.Zergling;
import Core.Jugador;
public class Tablero implements Runnable {
	/**
	 * Est� pensado para coordenadas de la siguiente forma: (0,0) |------------------| (w,0) |					| |					| | 					| (0,h) |------------------| (w,h)
	 * @uml.property  name="entidades"
	 * @uml.associationEnd  multiplicity="(0 -1)" inverse="tablero:Entidades.Entidad"
	 */
	public List<Entidad> entidades;
	/**
	 * @uml.property  name="ancho"
	 */
	public final int ancho;
	/**
	 * @uml.property  name="alto"
	 */
	public final int alto; 
	/**
	 * @uml.property  name="celulas" multiplicity="(0 -1)" dimension="2"
	 */
	public Celula[][] celulas;
	/**
	 * @uml.property  name="jugador"
	 * @uml.associationEnd  inverse="tablero:Core.Jugador"
	 */
	private Jugador  jugador;
	/**
	 * @uml.property  name="gameover"
	 */
	private boolean gameover = false;
	/**
	 * @author     macbook
	 */
	public enum facciones  {/**
	 * @uml.property  name="terricola"
	 * @uml.associationEnd  
	 */
	Terricola, /**
	 * @uml.property  name="zerg"
	 * @uml.associationEnd  
	 */
	Zerg, /**
	 * @uml.property  name="neutral"
	 * @uml.associationEnd  
	 */
	Neutral }
	/**
	 * @uml.property  name="semaModEntidades"
	 */
	public Semaphore semaModEntidades = new Semaphore(1);
	/**
	 * @uml.property  name="semaModEntidadesPendientes"
	 */
	public Semaphore semaModEntidadesPendientes = new Semaphore(1);
	/**
	 * @uml.property  name="entidadesPendientes"
	 * @uml.associationEnd  multiplicity="(0 -1)" elementType="Entidades.Entidad"
	 */
	private List<Entidad> entidadesPendientes;
	/**
	 * @uml.property  name="threadAgregadorDePendientes"
	 */
	private Thread threadAgregadorDePendientes = null;

	
	/**
	 * @uml.property  name="posxma"
	 */
	private int posxma;
	/**
	 * @uml.property  name="posyma"
	 */
	private int posyma;
	
	public Tablero(int ancho, int alto){
		this.ancho = ancho;
		this.alto = alto;
		entidadesPendientes = new ArrayList<Entidad>();
		entidades = new ArrayList<Entidad>();
		celulas = new Celula[ancho][alto];
		threadAgregadorDePendientes = new Thread(this, "tablero");
		threadAgregadorDePendientes.start();
		
	}

	
	/**
	 * Agrega una entidad al tablero. Si no logra agregarla por colisión, intentará agregarla más adelante.
	 * @param ent Entidad a agregar.
	 * @return Retorna true si logró agregarla, false en caso contrario (hubo colisión).
	 */
	public boolean agregarEntidad(Entidad ent){
		
		boolean colision = false;
		try {
			colision = colisiona(ent);
			if(!colision){
				semaModEntidades.acquire();
				entidades.add(ent);
				for (Iterator<Celula> i = ent.celulas.iterator(); i.hasNext();){ // foreach en Java
					Celula currCelula = i.next();
					celulas[currCelula.pos.x%ancho][currCelula.pos.y%alto] = currCelula; 
				}
				ent.start();
				colision = false;
				semaModEntidades.release();
			}
			else if(entidadesPendientes.contains(ent) == false){ // Si colisiona, la metemos en la lista para agregarla más adelante
				semaModEntidadesPendientes.acquire();
				entidadesPendientes.add(ent);
				semaModEntidadesPendientes.release();
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return !colision;
		
	}
	
	public boolean colisiona(Entidad ent){
		for (Iterator<Celula> i = ent.celulas.iterator(); i.hasNext();){ // foreach en Java
			Celula currCelula = i.next();
            if(celulas[currCelula.pos.x%ancho][currCelula.pos.y%alto] != null){
            	return  true;
            		
            }
		}
		return false;
	}
	
	@Override
	public void run() {
		while(gameover == false){
			Entidad entQueYaNoEsPendiente = null;
			try {
				semaModEntidadesPendientes.acquire();
				for(Entidad e : entidadesPendientes){
					if(agregarEntidad(e)){
						entQueYaNoEsPendiente = e;
						break;
					}
					
						
				}
				semaModEntidadesPendientes.release();
				if(entQueYaNoEsPendiente != null){
					entidadesPendientes.remove(entQueYaNoEsPendiente);
				}
				
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
	
	public void eliminarEntidad(Entidad ent){
		try {
			semaModEntidades.acquire();
			entidades.remove(ent);
			for (Iterator<Celula> i = ent.celulas.iterator(); i.hasNext();){ // foreach en Java
				Celula currCelula = i.next();
				celulas[currCelula.pos.x][currCelula.pos.y] = null; 
			}
		
			semaModEntidades.release();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public List<Entidad> getEntidadesCercanas(Tuple2D<Integer, Integer> posDesde, double radio )
	{
		List<Entidad> result = new ArrayList<Entidad>();
		// FIXME: Este m�todo est� recorriendo todo el tablero (O(WxH)) y no es necesario hacerlo. Optimizar este m�todo.
		for(int y = 0; y < alto; y++){
			for(int x = 0; x < ancho; x++){
				Tuple2D<Integer, Integer> currPos = new Tuple2D<Integer, Integer>(x, y);
				if(Utilidad.distance(posDesde, currPos, this) <= radio && celulas[x][y] != null	)
				{
					result.add(celulas[x][y].entidadPadre);
				}
			}
		}
		return result;
	}
	
	public Celula getCelulaEnPosicion(Integer x, Integer y){		
		return celulas[x][y];		
	}
	
	public void eliminarPosicionEntidad(Entidad ent){
		try {
			semaModEntidades.acquire();
			for (Iterator<Celula> i = ent.celulas.iterator(); i.hasNext();){ 
				Celula currCelula = i.next();
				celulas[currCelula.pos.x][currCelula.pos.y] = null; 
			}		
			semaModEntidades.release();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void insertarPosicionEntidad(Entidad ent){
		try {
			semaModEntidades.acquire();
			for (Iterator<Celula> i = ent.celulas.iterator(); i.hasNext();){ 
				Celula currCelula = i.next();
				celulas[currCelula.pos.x][currCelula.pos.y] = currCelula; 
			}
			semaModEntidades.release();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void printEnConsola(){
		
		for(int y = 0; y < alto; y++){
			System.out.print("|");
			for(int x = 0; x < ancho; x++){
				if(celulas[x][y] != null)
					System.out.print(celulas[x][y].entidadPadre.toString());
				else
					System.out.print(" ");
				System.out.print("|");		
			}
			System.out.println();
			
		}
		
	}


	
	/**
	 * Necesitaba obtener el jugador para que los edificios supieran a quen deven
	 * dar los recursos
	 * */
	public void set_jugador(Jugador j){
		this.jugador =j;
	}
	public Jugador get_jugador(){
		return this.jugador;
	}
	



	
	
	//Get and Set de la posicion de la Madriguera para crear enemigos
	/**
	 * @return
	 * @uml.property  name="posyma"
	 */
	public int getPosyma() {
		return posyma;
	}
	/**
	 * @param posyma
	 * @uml.property  name="posyma"
	 */
	public void setPosyma(int posyma) {
		this.posyma = posyma;
	}
	

	/**
	 * @return
	 * @uml.property  name="posxma"
	 */
	public int getPosxma() {
		return posxma;
	}

	/**
	 * @param posxma
	 * @uml.property  name="posxma"
	 */
	public void setPosxma(int posxma) {
		this.posxma = posxma;
	}

	
}
