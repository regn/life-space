package Core;

import javax.swing.border.Border;

import Entidades.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class TableroBuilder {
	/**
	 * @uml.property  name="tablero"
	 * @uml.associationEnd  readOnly="true"
	 */
	public Tablero tablero;
	
	public static Tablero build(int dificultad , int dimx , int dimy, boolean cargar){
		// Creamos un tablero
	
		Tablero tablero =null;
		if(cargar==false){	
			
		tablero = new Tablero( dimx, dimy);
		if(dificultad==1){
	    TableroBuilder.Cargar_facil(tablero);
		}else if(dificultad==2){
	    TableroBuilder.Cargar_Medio(tablero);
		}else if(dificultad==3){
	    TableroBuilder.Cargar_Dificil(tablero);
		}
		
		Jugador j = new Jugador(tablero, 20,20,20);
		tablero.set_jugador(j);
		
		}else{
		
		tablero = TableroBuilder.Cargar(dimx, dimy);
		Jugador j = new Jugador(tablero, 20,20,20);
		tablero.set_jugador(j);
			
		}
		return tablero;
	}
	
	public  static Tablero Cargar(int dimx, int dimy){
		Tablero t  = new Tablero(dimx, dimy);
		
		
		File f = new File( "../Partidas/Partida.txt" );
		BufferedReader entrada;
		try {
		entrada = new BufferedReader( new FileReader( f ) );
		String linea;
		while(entrada.ready()){
		linea = entrada.readLine();
		String datos[] = linea.split(" ");
		
		int posx =  Integer.parseInt(datos[1]);
		int posy =  Integer.parseInt(datos[2]);
		
		String tipo =  datos[0];
		
		//ir agregando entidades segun su tipo
		if(tipo.equalsIgnoreCase("MU")){
			Muro m = new Muro("M", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
			
		}else if(tipo.equalsIgnoreCase("AU")){
			Aumentador m = new Aumentador("A", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
			
		}else if(tipo.equalsIgnoreCase("BA")){
			Barraca m = new Barraca("B", t,posx,posy);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
			
		}else if(tipo.equalsIgnoreCase("AC")){
			Acero m = new Acero("AC", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
			
		}else if(tipo.equalsIgnoreCase("CE")){
			CentroUrbano m = new CentroUrbano("C", t,posx,posy);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
			
		}else if(tipo.equalsIgnoreCase("ZE")){
			Zergling m = new Zergling("z", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
			
		}else if(tipo.equalsIgnoreCase("AL")){
			Aldeano m = new Aldeano("AL", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
			
		}else if(tipo.equalsIgnoreCase("ME")){
			MarinoEspacial m = new MarinoEspacial("me", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
		
		}else if(tipo.equalsIgnoreCase("MA")){
			Madriguera m = new Madriguera("ma", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
	
		}else if(tipo.equalsIgnoreCase("PL")){
			Plasma m = new Plasma("pl", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
		
		}else if(tipo.equalsIgnoreCase("TA")){
			TanqueEspacial m = new TanqueEspacial("Ta", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
		
		}else if(tipo.equalsIgnoreCase("TR")){
			Trampa m = new Trampa("Tr", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
		
		}else if(tipo.equalsIgnoreCase("UR")){
			Uranio m = new Uranio("U", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
		}else if(tipo.equalsIgnoreCase("HI")){
			Hidralisco m = new Hidralisco("Hi", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
		}
		else if(tipo.equalsIgnoreCase("RB")){
			RobotDeBatalla m = new RobotDeBatalla("ro", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
		}
		else if(tipo.equalsIgnoreCase("UL")){
			Ultralisco m = new Ultralisco("Ul", t);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx ,posy ));
			t.agregarEntidad(m);
		}
		
		
		
		
		
		}
		}catch (IOException e) {
		e.printStackTrace();
		}

		return t;
	} 
	public static void Guardar(Tablero tablero){
		try{
		File file = new File("../Partidas/Partida.txt");
		 
		String partida = "";
		
		for (Iterator<Entidad> i = tablero.entidades.iterator(); i.hasNext();){
			Entidad curentidad = i.next();
			
			Tuple2D<Integer , Integer > pocicion = curentidad.referencia;
			partida= partida+curentidad.n_guardar+ " "+pocicion.x +" "+pocicion.y+" \n";
			}
		
		
			file.createNewFile();
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(partida);
			bw.close();
			
		
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void Cargar_facil(Tablero tablero){
		
		int centrox = tablero.ancho/2; 
		int centroy = tablero.alto/2;
		//cargar un centro urbano
		CentroUrbano centro = new CentroUrbano("C", tablero,centrox,centroy);
		centro.posicionarInicialmente(new Tuple2D<Integer, Integer>(centrox, centroy));
		tablero.agregarEntidad(centro);
		
		//cargar aumentadors
		Aumentador aumenta1 = new Aumentador("A", tablero);
		aumenta1.posicionarInicialmente(new Tuple2D<Integer, Integer>(centrox - 6, centroy));
		tablero.agregarEntidad(aumenta1);
		
		Aumentador aumenta2= new Aumentador("A", tablero);
		aumenta2.posicionarInicialmente(new Tuple2D<Integer, Integer>(centrox, centroy+6));
		tablero.agregarEntidad(aumenta2);
		
		Aumentador aumenta3 = new Aumentador("A", tablero);
		aumenta3.posicionarInicialmente(new Tuple2D<Integer, Integer>(centrox + 7, centroy));
		tablero.agregarEntidad(aumenta3);
		
		Aumentador aumenta4 = new Aumentador("A", tablero);
		aumenta4.posicionarInicialmente(new Tuple2D<Integer, Integer>(centrox , centroy-5));
		tablero.agregarEntidad(aumenta4);
		int distcentrox;
		int distcentroy;
		int murallas = 20;
		
		//crear murallas
		for(int i  =0 ; i<murallas; i++){
			
			distcentrox=7;
			distcentroy=7;
			int iniciox=centrox-distcentrox;
			int inicioy=centroy-distcentroy;
			Muro m = new Muro("M", tablero);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(iniciox+i ,inicioy ));
			tablero.agregarEntidad(m);
			
			distcentrox=-7;
			distcentroy=10;
			iniciox=centrox+distcentrox;
			inicioy=centroy+distcentroy;
			m = new Muro("M", tablero);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(iniciox+i ,inicioy ));
			tablero.agregarEntidad(m);
			
			distcentrox=-12;
			distcentroy=-8;
			iniciox=centrox+distcentrox;
			inicioy=centroy+distcentroy;
			m = new Muro("M", tablero);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(iniciox ,inicioy+i ));
			tablero.agregarEntidad(m);
			
			distcentrox=+16;
			distcentroy=-8;
			iniciox=centrox+distcentrox;
			inicioy=centroy+distcentroy;
			m = new Muro("M", tablero);
			m.posicionarInicialmente(new Tuple2D<Integer, Integer>(iniciox ,inicioy+i ));
			tablero.agregarEntidad(m);
		
		}
		
		//cargar barracas
		distcentrox=+19;
		distcentroy=0;
		int posx=centrox+distcentrox;
		int posy=centroy+distcentroy;
		Barraca Mesp = new Barraca("B", tablero,posx,posy);
		Mesp.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx, posy));
		tablero.agregarEntidad(Mesp);
		
		distcentrox=0;
		distcentroy=-17;
		posx=centrox+distcentrox;
	    posy=centroy+distcentroy;
		Mesp = new Barraca("B", tablero,posx,posy);
		Mesp.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx, posy));
		tablero.agregarEntidad(Mesp);
		
		distcentrox=-17;
		distcentroy= 0;
		posx=centrox+distcentrox;
	    posy=centroy+distcentroy;
		Mesp = new Barraca("B", tablero,posx,posy);
		Mesp.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx, posy));
		tablero.agregarEntidad(Mesp);
		
		distcentrox=0;
		distcentroy=+17;
		posx=centrox+distcentrox;
	    posy=centroy+distcentroy;
		Mesp = new Barraca("B", tablero,posx,posy);
		Mesp.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx, posy));
		tablero.agregarEntidad(Mesp);
		
		TableroBuilder.Cargar_madrigueras(tablero);
		
	    }
	private static void Cargar_Medio(Tablero tablero) {
		
		int centrox = tablero.ancho/2; 
		int centroy = tablero.alto/2;
		
	
		CentroUrbano centro = new CentroUrbano("C", tablero,centrox,centroy);
		centro.posicionarInicialmente(new Tuple2D<Integer, Integer>(centrox, centroy));
		tablero.agregarEntidad(centro);
		
		int distcentrox=-7;
		int distcentroy=0;
		int posx=centrox+distcentrox;
	    int posy=centroy+distcentroy;
		Aumentador aumenta1 = new Aumentador("A", tablero);
		aumenta1.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx, posy));
		tablero.agregarEntidad(aumenta1);
		
		distcentrox=+7;
	    distcentroy=0;
		posx=centrox+distcentrox;
	    posy=centroy+distcentroy;
		Aumentador aumenta2= new Aumentador("A", tablero);
		aumenta2.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx, posy));
		tablero.agregarEntidad(aumenta2);
		
		distcentrox=0;
	    distcentroy=+7;
		posx=centrox+distcentrox;
	    posy=centroy+distcentroy;
		Barraca barr= new Barraca ("B", tablero,posx,posy);
		barr.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx, posy));
		tablero.agregarEntidad(barr);
		
		distcentrox=0;
	    distcentroy=-7;
		posx=centrox+distcentrox;
	    posy=centroy+distcentroy;
	     barr= new Barraca ("B", tablero,posx,posy);
		barr.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx, posy));
		tablero.agregarEntidad(barr);
		
		TableroBuilder.Cargar_madrigueras(tablero);
	}
	private static void Cargar_Dificil(Tablero tablero){
		
		int centrox = tablero.ancho/2; 
		int centroy = tablero.alto/2;
		CentroUrbano centro = new CentroUrbano("C", tablero,centrox,centroy);
		centro.posicionarInicialmente(new Tuple2D<Integer, Integer>(centrox, centroy));
		tablero.agregarEntidad(centro);
		
		int distcentrox=0;
	    int distcentroy=+7;
		int posx=centrox+distcentrox;
	    int posy=centroy+distcentroy;
		Barraca barr= new Barraca ("B", tablero,posx,posy);
		barr.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx, posy));
		tablero.agregarEntidad(barr);
		
		distcentrox=0;
	    distcentroy=-7;
		posx=centrox+distcentrox;
	    posy=centroy+distcentroy;
	     barr= new Barraca ("B", tablero,posx,posy);
		barr.posicionarInicialmente(new Tuple2D<Integer, Integer>(posx, posy));
		tablero.agregarEntidad(barr);
		
		TableroBuilder.Cargar_madrigueras(tablero);
		
	}
    private static void Cargar_madrigueras(Tablero tablero){
    	
    	int centrox = tablero.ancho/2; 
		int centroy = tablero.alto/2;
    	int distbordex=centrox;
        int distbordey=centroy;
    	int posx = distbordex;
    	int posy = distbordey;
    	Madriguera m = new Madriguera("M", tablero);
    	m.posicionarInicialmente(new Tuple2D<Integer, Integer>(4,centrox));
    	tablero.agregarEntidad(m);
    	tablero.setPosxma(0);
    	tablero.setPosyma(40);

    }
}
