package Core;

import java.util.Random;

public class Utilidad {
	public static double distance(Tuple2D<Integer, Integer> pos1, Tuple2D<Integer,Integer> pos2 , Tablero tablero ){
		
		double d1=  (pos1.x - pos2.x)*(pos1.x - pos2.x) + (pos1.y - pos2.y)*(pos1.y - pos2.y) ;
	    
		double pos1x = (pos1.x +tablero.ancho/2)%tablero.ancho;
		double pos2x = (pos2.x +tablero.ancho/2)%tablero.ancho;
		double pos1y = (pos1.y +tablero.alto/2)%tablero.alto;
		double pos2y = (pos2.y +tablero.alto/2)%tablero.alto; 
		double d2=   (pos1x - pos2x)*(pos1x - pos2x) + (pos1y - pos2y)*(pos1y - pos2y) ;
		
	    
		if(d1<d2){return d1;}
		else{return d2;}	
	}
	
	public static Random random = new Random();

  	public static int RandomInt(int min, int max){
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}
}
