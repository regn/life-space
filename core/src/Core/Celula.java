package Core;

import java.util.HashMap;
import java.util.Map;

import Entidades.Entidad;
import com.badlogic.gdx.graphics.Color;


public class Celula{
	/**
	 * @uml.property  name="pos"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	public Tuple2D<Integer, Integer> pos;
	/**
	 * @uml.property  name="entidadPadre"
	 * @uml.associationEnd  multiplicity="(1 1)" inverse="celulas:Entidades.Entidad"
	 */
	public Entidad entidadPadre;
	/**
	 * @uml.property  name="color"
	 * @uml.associationEnd  qualifier="key:Core.Celula$colores com.badlogic.gdx.graphics.Color"
	 */
	public Color color;
	/**
	 * @author     macbook
	 */
	public enum colores  { /**
	 * @uml.property  name="negro"
	 * @uml.associationEnd  
	 */
	negro, /**
	 * @uml.property  name="azul"
	 * @uml.associationEnd  
	 */
	azul, /**
	 * @uml.property  name="cyan"
	 * @uml.associationEnd  
	 */
	cyan, /**
	 * @uml.property  name="gris_oscuro"
	 * @uml.associationEnd  
	 */
	gris_oscuro, /**
	 * @uml.property  name="gris"
	 * @uml.associationEnd  
	 */
	gris, /**
	 * @uml.property  name="verde"
	 * @uml.associationEnd  
	 */
	verde, /**
	 * @uml.property  name="gris_claro"
	 * @uml.associationEnd  
	 */
	gris_claro, /**
	 * @uml.property  name="magenta"
	 * @uml.associationEnd  
	 */
	magenta, /**
	 * @uml.property  name="naranjo"
	 * @uml.associationEnd  
	 */
	naranjo, /**
	 * @uml.property  name="rosado"
	 * @uml.associationEnd  
	 */
	rosado, /**
	 * @uml.property  name="rojo"
	 * @uml.associationEnd  
	 */
	rojo, /**
	 * @uml.property  name="blanco"
	 * @uml.associationEnd  
	 */
	blanco, /**
	 * @uml.property  name="amarillo"
	 * @uml.associationEnd  
	 */
	amarillo}
	private static Map<colores, Color> enumToColor;
	public Celula(Tuple2D<Integer, Integer> posicion, Entidad entidadPadre, colores color) {
		// Si el diccionario está vacío, rellenarlo por primera vez
		if(enumToColor == null){
			enumToColor = new HashMap<colores, Color>();
			enumToColor.put(colores.negro, Color.BLACK);
			enumToColor.put(colores.azul, Color.BLUE);
			enumToColor.put(colores.cyan, Color.CYAN);
			enumToColor.put(colores.gris_oscuro, Color.DARK_GRAY);
			enumToColor.put(colores.gris, Color.GRAY);
			enumToColor.put(colores.verde, Color.GREEN);
			enumToColor.put(colores.gris_claro, Color.LIGHT_GRAY);
			enumToColor.put(colores.magenta, Color.MAGENTA);
			enumToColor.put(colores.naranjo, Color.ORANGE);
			enumToColor.put(colores.rosado, Color.PINK);
			enumToColor.put(colores.rojo, Color.RED);
			enumToColor.put(colores.blanco, Color.WHITE);
			enumToColor.put(colores.amarillo, Color.YELLOW);
		}
		
		pos = posicion;
		this.entidadPadre = entidadPadre;
		this.color = enumToColor.get(color);
		
	}
	
	public void setColor(colores color){
		this.color = enumToColor.get(color);
	}

}
