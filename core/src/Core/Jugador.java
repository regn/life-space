package Core;

import Entidades.Entidad;
import Entidades.Entidad.Recursos;
import Entidades.Acero;
import Entidades.Muro;
import Entidades.Recurso;

public class Jugador {

	
	/**
	 * @uml.property  name="acero"
	 */
	private int Acero;
	/**
	 * @uml.property  name="plasma"
	 */
	private int Plasma;
	/**
	 * @uml.property  name="uranio"
	 */
	private int Uranio;
	/**
	 * @uml.property  name="puntaje"
	 */
	private int Puntaje;
	/**
	 * @uml.property  name="tablero"
	 * @uml.associationEnd  multiplicity="(1 1)" inverse="jugador:Core.Tablero"
	 */
	private Tablero tablero;
	/**
	 * @uml.property  name="precioAcero"
	 */
	private int precioAcero;
	/**
	 * @uml.property  name="precioUranio"
	 */
	private int precioUranio;
	/**
	 * @uml.property  name="precioPlasma"
	 */
	private int precioPlasma;
	public Jugador(Tablero tablero ,int acero_inicial , int plasma_inicial , int uranio_inicial  ){
		
		this.Acero = acero_inicial;
		this.Plasma = plasma_inicial;
		this.Uranio = uranio_inicial;
		this.precioAcero = 1;
		this.precioPlasma = 1;
		this.precioUranio = 1;
		this.Puntaje=0;
		this.tablero = tablero;
	}
	
	public int getPuntaje(){
		return Puntaje;
	}
	
	/**Suma o resta la cantidad ingresada , retorna true si el valor fue cambiado,
	 * Si con el cambio la cantidad de recurso se hace negativa devuelve false
	 * y no realisa el cambio*/
	public boolean modificarAcero(int cantidad){

		int Actual  = this.Acero;
		int nuevo = Actual+ cantidad;
		if(nuevo >=0){
			this.Acero =nuevo;
			return true;
		}else{
			return false;	
			}
		}
	public boolean modificarUranio(int cantidad){
		int Actual  = this.Uranio;
		int nuevo = Actual+ cantidad;
		if(nuevo >=0){
			this.Uranio =nuevo;
			return true;
		}else{
			return false;
			}
		}
	public boolean modificarPlasma(int cantidad){
		int Actual  = this.Plasma;
		int nuevo = Actual+ cantidad;
		if(nuevo >=0){
			this.Plasma =nuevo;
			return true;
		}else{
			return false;
			}
		}
	public boolean modificarPuntaje(int cantidad){
	
		
		int Actual  = this.Puntaje;
		int nuevo = Actual+ cantidad;
		if(nuevo >=0){
			this.Puntaje =nuevo;
			return true;
		}else{
			return false;
			}
		
	}
	
	/**
	 * Poner recurso  en el tablero
	 *
	 * */
	public boolean ponerRecurso(Recursos tipo , Tuple2D<Integer, Integer> posicion){
		
		
		if(tipo ==Recursos.Acero){
			if(modificarAcero(-precioAcero)){
			 Entidades.Acero a  = new Acero("A", this.tablero);
             a.posicionarInicialmente(posicion);
             tablero.agregarEntidad(a);
             return true;
			}else {return false;}
		}
		else if(tipo ==Recursos.Uranio){
			if(modificarUranio(-precioUranio)){
			 Entidades.Uranio u = new Entidades.Uranio("U", tablero);
             u.posicionarInicialmente(posicion);
             tablero.agregarEntidad(u);
             return true;
			}else {return false;}
		}
		else if(tipo ==Recursos.Plasma){
			if(modificarPlasma(-precioPlasma)){
			 Entidades.Plasma p = new Entidades.Plasma("P", tablero);
             p.posicionarInicialmente(posicion);
             tablero.agregarEntidad(p);
             return true;
			}else {return false;}
		}
		return false;
	}
	
	
	/**
	 * @return
	 * @uml.property  name="acero"
	 */
	public int getAcero() {
		return Acero;
	}

	/**
	 * @return
	 * @uml.property  name="plasma"
	 */
	public int getPlasma() {
		return Plasma;
	}

	/**
	 * @return
	 * @uml.property  name="uranio"
	 */
	public int getUranio() {
		return Uranio;
	}
	
	
		
	}

