package Entidades;

import java.util.Iterator;
import java.util.List;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Utilidad;
import Core.Tablero.facciones;


public class Trampa extends Militar {

    boolean ataco; //necesario porque debe hacer daño a todos los enemigos que encontro en su radio de ataque

	public Trampa(String nombre, Tablero tablero) {
		super(nombre, tablero);
		radioVision = 1;
		radioAtaque = 1;
		puntosVida  = 100;  //es muy dificil de matar, si muere es porque exploto, haciendo daño a alguien
		ataque		= 10;
		faccion		= facciones.Terricola;
		ataco = false;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this)); // ocupa una celula
	}

		@Override
	protected void interactuar() {
		super.interactuar();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioAtaque);

		if(cooldownInteractuar > 0)
			return;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){ // foreach en Java
			Entidad e = i.next();
			if(e.faccion == Tablero.facciones.Zerg)
			{
				e.atacar(this);
				ataco = true;
			}			
		}
		if(ataco)
			matar();



	}
	

}