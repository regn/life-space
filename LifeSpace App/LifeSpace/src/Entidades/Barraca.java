package Entidades;

import java.util.*;
import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Tablero.facciones;


/*  FORMA PROVISIONAL BARRACAS
 *        OOO
 *       O O O
 *        OOO 
 * 
 */
public class Barraca extends Edificio {

	public Barraca(String nombre, Tablero tablero) {
		super(nombre, tablero);
		faccion = facciones.Terricola;
		puntosVida = 150;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,1), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,2), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,3), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,0), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,2), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,4), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,2), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,3), this));
		
	}
	
	
	@Override
	protected void interactuar(){
		//creacion de marinos
		Random random = new Random();
		MarinoEspacial m = new MarinoEspacial("m", tablero);
		int x= random.nextInt(tablero.alto-1);
		int y= random.nextInt(tablero.ancho-1);
	    //Revisar posicion posible para agregar a entidad
		m.posicionarInicialmente(new Tuple2D<Integer, Integer>(x,y));

		
		tablero.AgregarEntidad(m);
	}

}
