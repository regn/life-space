package Entidades;

import Core.Tablero;
import Core.Tuple2D;

public class Recurso extends Entidad {
   
	protected Recursos tipoRecurso;
	
	public Recurso(String nombre, Tablero tablero) {
		super(nombre, tablero);
	}
	public Recursos get_tipoRecurso(){
		return tipoRecurso;
	}
	public void posicionarInicialmente(Tuple2D<Integer, Integer> pos){
		super.posicionarInicialmente(pos);
	}
}
