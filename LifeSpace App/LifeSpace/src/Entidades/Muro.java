package Entidades;

import Core.Tablero;
import Core.Celula;
import Core.Tuple2D;
import Core.Tablero.facciones;

/* No hace nada mas que recibir daño e impedir el paso de otras entidades */
public class Muro extends Edificio {

	public Muro(String nombre, Tablero tablero) {
		super(nombre, tablero);
		puntosVida = 50;	
		faccion = facciones.Terricola;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this));
		
	}

}
