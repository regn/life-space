package Entidades;

import java.util.Iterator;
import java.util.List;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Utilidad;
import Core.Tablero.facciones;

public class Zergling extends Militar {

	public Zergling(String nombre, Tablero tablero) {
		super(nombre, tablero);
		radioVision = 10;
		radioAtaque = 1;
		puntosVida  = 20;
		ataque		= 4;
		faccion		= facciones.Zerg;
		
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,0), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,1), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,1), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this));
	}
	
	@Override
	protected void interactuar() {
		super.interactuar();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioAtaque);
		/*
		 * Hasta ahora atacamos a la primera entidad terr�cola en rango de ataque.
		 * TODO: Mejorar la inteligencia del zergling.
		 */
		if(cooldownInteractuar > 0)
			return;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){ // foreach en Java
			Entidad e = i.next();
			if(e.faccion == Tablero.facciones.Terricola)
			{
				e.atacar(this);
				break;
			}
		}
	}
	
	@Override
	protected void mover() {
		super.mover();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioVision);
		/*
		 * Hasta ahora nos movemos a la entidad terr�cola m�s cercana.
		 * TODO: Mejorar la inteligencia del zergling.
		 */
		if(cooldownMoverse > 0)
			return;
		
		Entidad entAPerseguir = null;
		double menorRadio = radioVision;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){
			Entidad e = i.next();
			double distanciaHaciaEntidad = Utilidad.distance(getCentroMasa(), e.getCentroMasa());
			if(e.faccion == Tablero.facciones.Terricola 
					&& distanciaHaciaEntidad <= menorRadio )
			{
				menorRadio = distanciaHaciaEntidad;
				entAPerseguir = e;
			}
		}
		// Si hay una entidad a perseguir en el rango de visi�n del alien, estar� almacenada en entAPerseguir
		if(entAPerseguir != null){
			moverHacia(entAPerseguir.getCentroMasa());
		}
		
	}
	

}
