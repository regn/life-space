package Entidades;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Tablero.facciones;

public class Plasma extends Recurso {

	public Plasma(String nombre, Tablero tablero) {
		super(nombre, tablero);
		faccion = facciones.Neutral;	
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this));
		tipoRecurso = Recursos.Plasma;
	}

}
