package Entidades;

import java.util.*;
import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Tablero.facciones;

public class CentroUrbano extends Edificio {
	
	/* FORMA PROVISIONAL CENTRO URBANO
	 *        O
	 *       OOO   
	 *      OOOOO
	 *       OOO
	 *        O 
	 */

	public CentroUrbano(String nombre, Tablero tablero) {
		super(nombre, tablero);
		puntosVida = 500; //muy resistente	
		faccion = facciones.Terricola;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,2), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,1), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,2), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,3), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,0), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,1), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,2), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,3), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(2,4), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(3,1), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(3,2), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(3,3), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(4,2), this));
	}	

	@Override
	protected void interactuar(){
		//creacion de aldeanos	
		Random random = new Random();
		Aldeano a = new Aldeano("a", tablero);
		int x= random.nextInt(tablero.alto-1);
		
		int y= random.nextInt(tablero.ancho-1);
		a.posicionarInicialmente(new Tuple2D<Integer, Integer>(x,y));
		tablero.AgregarEntidad(a);
	}
	

}
