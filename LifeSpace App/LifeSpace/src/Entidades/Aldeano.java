package Entidades;

import java.util.Iterator;
import java.util.List;

import Core.Celula;
import Core.Tablero;
import Core.Tuple2D;
import Core.Utilidad;
import Core.Tablero.facciones;

public class Aldeano extends Trabajador {
    
	

	public Aldeano(String nombre, Tablero tablero) {
		super(nombre, tablero);
		this.radioVison=50;	
		this.radioInteraccion=1;
		puntosVida  = 4;
		faccion		= facciones.Terricola;
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(0,0), this));
		celulas.add(new Celula(new Tuple2D<Integer,Integer>(1,0), this));
		
	
	}
	@Override
	protected void mover() {
		/**
		 * Busca el recurso mas cercano dentro de su vision y se mueve hacia el
		 * */
		super.mover();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(),this.radioVison);
		if(cooldownMoverse > 0)
			return;
		
		Entidad entAPerseguir = null;
		double menorRadio = this.radioVison;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){
			Entidad e = i.next();
			double distanciaHaciaEntidad = Utilidad.distance(getCentroMasa(), e.getCentroMasa());
			if(e.faccion == Tablero.facciones.Neutral && 
				distanciaHaciaEntidad <= menorRadio )
			{
				menorRadio = distanciaHaciaEntidad;
				entAPerseguir = e;
			}
		}
		// Si hay una entidad a perseguir en el rango de visi�n del alien, estar� almacenada en entAPerseguir
		if(entAPerseguir != null){
			moverHacia(entAPerseguir.getCentroMasa());
		}
		
	}
	
	
	@Override
	protected void interactuar() {
		super.interactuar();
		List<Entidad> entidadesCercanas = tablero.getEntidadesCercanas(getCentroMasa(), radioInteraccion);
	
		if(cooldownInteractuar > 0)
			return;
		for (Iterator<Entidad> i = entidadesCercanas.iterator(); i.hasNext();){ // foreach en Java
			Entidad e = i.next();
			if(e.faccion == Tablero.facciones.Neutral)
			{
				this.construir(e);
				break;
			}
		}
	}
    
	@Override
	protected void construir(Entidad e) {
		/**
		 * dependiendo del recurso que edificio construye , luego destruye el aldeano
		 * se construye un edificio en donde estaba el recurso
		 * */
		
		Recurso r  = (Recurso)e;
		Edificio b;
		if(r.get_tipoRecurso() ==Recursos.Acero ){
			 b  = new Barraca("B", tablero);
		}else if(r.get_tipoRecurso() ==Recursos.Uranio) {
		     b = new Aumentador ("A", tablero);
		}else if(r.get_tipoRecurso() ==Recursos.Plasma) {
			 b  = new Muro("M", tablero);
		}else{b =null;}
		e.matar();
		Tuple2D< Integer, Integer> pocicion = r.getCentroMasa();
		b.posicionarInicialmente(pocicion);
	}

}
