package Entidades;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import Core.Celula;
import Core.Tablero.facciones;
import Core.Tuple2D;
import Core.Tablero;


public abstract class Entidad implements Runnable {
	/** tiempoAccion:double
	 *  Indica el tiempo que debe transcurrir para que la entidad realice una acci�n en milisegundos
	 */
	protected long tiempoAccion;
	protected boolean vivo;
	protected boolean started;
	public List<Celula> celulas;
	protected Tablero tablero;
	public int puntosVida;
	public Tablero.facciones faccion;
	protected String id;
	protected int ticksParaMoverse;
	protected int ticksParaInteractuar;
	protected int cooldownInteractuar;
	protected int cooldownMoverse;
	private Thread thread = null;
	public enum Recursos  {Acero, Plasma, Uranio }
	
	public Entidad(String nombre, Tablero tablero){
		id = nombre;
		faccion = facciones.Neutral;
		vivo = true;
		started = false;
		celulas = new ArrayList<Celula>();
		this.tablero = tablero;
		ticksParaMoverse = 1;
		ticksParaInteractuar = 1;
		cooldownMoverse = 1;
		cooldownInteractuar = 1;
	}
	
	public void start(){
		started = true;
		if(thread == null)
			thread = new Thread(this, id);
		thread.start();
	}
	
	public void detener(){
		started = false;
		thread.stop();
	}
	
	public boolean estaVivo(){
		return vivo;
	}
	
	public void matar(){
		vivo = false;
	}
	/**
	 * M�todo interactuar. Debe sobrescribirse con el comportamiento de la entidad concreta.
	 * Actualmente hace la l�gica del cooldown
	 */
	protected void interactuar(){
		cooldownInteractuar--;
		if(cooldownInteractuar < 0)
			cooldownInteractuar = ticksParaInteractuar;
	}
	
	/**
	 * M�todo mover. Debe sobrescribirse con el comportamiento de la entidad conctreta. 
	 * Actualmente hace la l�gica del cooldown
	 */
	protected void mover(){
		cooldownMoverse--;
		if(cooldownMoverse < 0)
			cooldownMoverse = ticksParaMoverse;
	}
	
	/**
	 * Metodo que se llama al crear el Thread. Este m�todo corre en un thread aparte.
	 * No deber�a ser necesario sobrescribir este m�todo.
	 */
	public void run(){
		// Iteramos mientras est� vivo
		while(vivo){
			// Si no ha sido inicializado, esperamos
			if(started == false)
				continue;
			
			// Esperamos a que est� listo para actuar
			try {
				Thread.sleep(tiempoAccion);
			} catch (InterruptedException e) {
				// TODO: Manejar excepci�n
			}
			
			// Movemos e interactuamos
			mover();
			interactuar();
		}
		
	}
	/**
	 * M�todo de utilidad que retorna la posicion del centro de la entidad.
	 * @return Tupla que contiene la posicion central de la entidad.
	 */
	protected Tuple2D<Integer,Integer> getCentroMasa(){
		int numCelulasX = 0, numCelulasY = 0, totalPosX = 0, totalPosY = 0;
		for (Iterator<Celula> i = celulas.iterator(); i.hasNext();){ // foreach en Java
			Celula currCelula = i.next();
			numCelulasX++;
			numCelulasY++;
			totalPosX += currCelula.pos.x;
			totalPosY += currCelula.pos.y;
		}
		int resultX = (int)(totalPosX / numCelulasX);
		int resultY = (int)(totalPosY / numCelulasY);
		Tuple2D<Integer,Integer> result = new Tuple2D<Integer, Integer>(resultX, resultY);
		return result;
	}
	
	public void atacar(Militar atacante){
		// TODO: Validar que el atacante est� en el rango del atacado
		puntosVida -= atacante.ataque;
		if(puntosVida <= 0)
			matar();
	}
	
	protected void construir(Entidad e){
		
		
	}
	
	/**
	 * M�todo auxiliar para mover la entidad hacia la posici�n objetivo. No valida que no se haya movido este tick.
	 * @param destino
	 */
	protected void moverHacia(Tuple2D<Integer, Integer> destino){
		Tuple2D<Integer,Integer> currPos = getCentroMasa();
		Tuple2D<Integer,Integer> desp = 
				new Tuple2D<Integer, Integer>(destino.x - currPos.x, destino.y - currPos.y);
		if(Math.abs(desp.x) > Math.abs(desp.y)){
			// Debemos acortar la distancia horizontal.
			int despX = (int)Math.signum(desp.x);
			moverAux(despX, true);
		}
		else{
			int despY = (int)Math.signum(desp.y);
			moverAux(despY, false);
		}
	}
	
	private void moverAux(int ammount, boolean esMovHorizontal){
		
		// TODO Controlar colisiones!
		tablero.eliminarPosicionEntidad(this);
		
		boolean colision = false;
		
		for (Iterator<Celula> i = celulas.iterator(); i.hasNext();){ // foreach en Java
			Celula currCelula = i.next();
            if(esMovHorizontal && tablero.getCelulaEnPosicion((currCelula.pos.x+ammount)%tablero.ancho, currCelula.pos.y) != null){
            	colision = true;
            	break;
            }
            else if (tablero.getCelulaEnPosicion(currCelula.pos.x, (currCelula.pos.x+ammount)%tablero.ancho) != null){
            	colision = true;
            	break;
            }
			
		}
		if (!colision){
		for (Iterator<Celula> i = celulas.iterator(); i.hasNext();){ // foreach en Java
			Celula currCelula = i.next();
			if(esMovHorizontal){
				currCelula.pos.x += ammount;
				if(currCelula.pos.x < 0)
					currCelula.pos.x += tablero.ancho; 
					
				currCelula.pos.x %= tablero.ancho; // Asegurarse de que no se escape fuera del tablero
				
			}
			else {
				currCelula.pos.y += ammount;
				if(currCelula.pos.y < 0)
					currCelula.pos.y += tablero.alto; 	
				currCelula.pos.y %= tablero.alto; // Asegurarse de que no se escape fuera del tablero		
			}
			
		}		
		}
		tablero.insertarPosicionEntidad(this);
		
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return id;
	}
	
	public void posicionarInicialmente(Tuple2D<Integer, Integer> pos){
		for (Iterator<Celula> i = celulas.iterator(); i.hasNext();){ // foreach en Java
			Celula currCelula = i.next();
			currCelula.pos.x += pos.x;
			currCelula.pos.y += pos.y;
		}
	}
}