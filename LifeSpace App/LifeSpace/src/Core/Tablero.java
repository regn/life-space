package Core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;

import Core.Tuple2D;
import Entidades.Entidad;
import Core.Jugador;
public class Tablero {
	/**
	 * Est� pensado para coordenadas de la siguiente forma:
	 * 
	 * (0,0) |------------------| (w,0)
	 *       |					|
	 *       |					|
	 *       | 					|
	 * (0,h) |------------------| (w,h)
	 */
	private List<Entidad> entidades;
	public final int ancho;
	public final int alto; 
	private Celula[][] celulas;
	private Jugador  jugador;
	public enum facciones  {Terricola, Zerg, Neutral }
	
	public Tablero(int ancho, int alto){
		this.ancho = ancho;
		this.alto = alto;
		entidades = new ArrayList<Entidad>();
		celulas = new Celula[ancho][alto];
	}
	
	public void AgregarEntidad(Entidad ent){
<<<<<<< HEAD
		
		boolean colision = false;
		
		for (Iterator<Celula> i = ent.celulas.iterator(); i.hasNext();){ // foreach en Java
			Celula currCelula = i.next();
            if(celulas[currCelula.pos.x][currCelula.pos.y] != null){
            	colision = true;
            	break;			
            }
		}
		
		if(!colision){
=======
>>>>>>> 5d269e4f03718d0bbd9efcf73e703c167bb98ff3
		entidades.add(ent);
			
		for (Iterator<Celula> i = ent.celulas.iterator(); i.hasNext();){ // foreach en Java
			Celula currCelula = i.next();
			celulas[currCelula.pos.x][currCelula.pos.y] = currCelula; 
		}
		
		
		//TODO: Validar que no colisione con otras entidades
	}
	
	
	public List<Entidad> getEntidadesCercanas(Tuple2D<Integer, Integer> posDesde, double radio )
	{
		List<Entidad> result = new ArrayList<Entidad>();
		// FIXME: Este m�todo est� recorriendo todo el tablero (O(WxH)) y no es necesario hacerlo. Optimizar este m�todo.
		
		
		
		for(int y = 0; y < alto; y++){
			for(int x = 0; x < ancho; x++){
				Tuple2D<Integer, Integer> currPos = new Tuple2D<Integer, Integer>(x, y);
				if(Utilidad.distance(posDesde, currPos) <= radio && celulas[x][y] != null	)
				{
					result.add(celulas[x][y].entidadPadre);
				}
			}
		}
		return result;
	}
	
	public void eliminarPosicionEntidad(Entidad ent){
		for (Iterator<Celula> i = ent.celulas.iterator(); i.hasNext();){ 
			Celula currCelula = i.next();
			celulas[currCelula.pos.x][currCelula.pos.y] = null; 
		}		
	}
	
	public void insertarPosicionEntidad(Entidad ent){
		for (Iterator<Celula> i = ent.celulas.iterator(); i.hasNext();){ 
			Celula currCelula = i.next();
			celulas[currCelula.pos.x][currCelula.pos.y] = currCelula; 
		}
	}
	
	public void printEnConsola(){
		
		for(int y = 0; y < alto; y++){
			System.out.print("|");
			for(int x = 0; x < ancho; x++){
				if(celulas[x][y] != null)
					System.out.print(celulas[x][y].entidadPadre.toString());
				else
					System.out.print(" ");
				System.out.print("|");		
			}
			System.out.println();
			
		}
		
	}


	
	/**
	 * Necesitaba obtener el jugador para que los edificios supieran a quen deven
	 * dar los recursos
	 * */
	public void set_jugador(Jugador j){
		this.jugador =j;
	}
	public Jugador get_jugador(){
		return this.jugador;
	}
	
	
	
}
