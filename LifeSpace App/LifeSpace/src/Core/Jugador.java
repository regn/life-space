package Core;

import Entidades.Entidad;
import Entidades.Entidad.Recursos;
import Entidades.Acero;
import Entidades.Muro;
import Entidades.Recurso;

public class Jugador {

	private int Acero;
	private int Plasma;
	private int Uranio;
	private int Puntaje;
	private Tablero Tablero;
	private int precioAcero;
	private int precioUranio;
	private int precioPlasma;
	public Jugador(Tablero tablero ,int acero_inicial , int plasma_inicial , int uranio_inicial  ){
		
		this.Acero = acero_inicial;
		this.Plasma = plasma_inicial;
		this.Uranio = uranio_inicial;
		this.Puntaje=0;
		this.Tablero = tablero;
	}
	
	/**Suma o resta la cantidad ingresada , retorna true si el valor fue cambiado,
	 * Si con el cambio la cantidad de recurso se hace negativa devuelve false
	 * y no realisa el cambio*/
	public boolean modificarAcero(int cantidad){

		int Actual  = this.Acero;
		int nuevo = Actual+ cantidad;
		if(nuevo >=0){
			this.Acero =nuevo;
			return true;
		}else{
			return false;	
			}
		}
	public boolean modificarUranio(int cantidad){
		int Actual  = this.Uranio;
		int nuevo = Actual+ cantidad;
		if(nuevo >=0){
			this.Uranio =nuevo;
			return true;
		}else{
			return false;
			}
		}
	public boolean modificarPlasma(int cantidad){
		int Actual  = this.Plasma;
		int nuevo = Actual+ cantidad;
		if(nuevo >=0){
			this.Plasma =nuevo;
			return true;
		}else{
			return false;
			}
		}
	public boolean modificarPuntaje(int cantidad){
		int Actual  = this.Puntaje;
		int nuevo = Actual+ cantidad;
		if(nuevo >=0){
			this.Puntaje =nuevo;
			return true;
		}else{
			return false;
			}
		}
	
	/**
	 * Poner recurso  en el tablero
	 *
	 * */
	public boolean ponerRecurso(Recursos tipo , Tuple2D<Integer, Integer> pocicion){
		
		
		if(tipo ==Recursos.Acero){
			if(modificarAcero(-precioAcero)){
			 Entidades.Acero a  = new Acero("A", this.Tablero);
             a.posicionarInicialmente(pocicion);
             return true;
			}else {return false;}
		}
		else if(tipo ==Recursos.Uranio){
			if(modificarAcero(-precioUranio)){
			 Entidades.Uranio u = new Entidades.Uranio("U", Tablero);
             u.posicionarInicialmente(pocicion);
             return true;
			}else {return false;}
		}
		else if(tipo ==Recursos.Plasma){
			if(modificarAcero(-precioPlasma)){
			 Entidades.Plasma p = new Entidades.Plasma("P", Tablero);
             p.posicionarInicialmente(pocicion);
             return true;
			}else {return false;}
		}
		return false;
	}
	
	
	public int getAcero() {
		return Acero;
	}

	public int getPlasma() {
		return Plasma;
	}

	public int getUranio() {
		return Uranio;
	}
	
		
		
	}

