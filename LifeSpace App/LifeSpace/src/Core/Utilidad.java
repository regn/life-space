package Core;

public class Utilidad {
	public static double distance(Tuple2D<Integer, Integer> pos1, Tuple2D<Integer,Integer> pos2){
		return Math.sqrt( (pos1.x - pos2.x)*(pos1.x - pos2.x) + (pos1.y - pos2.y)*(pos1.y - pos2.y) );
	}
}
