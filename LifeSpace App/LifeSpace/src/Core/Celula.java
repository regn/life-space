package Core;

import Entidades.Entidad;



public class Celula{
	public Tuple2D<Integer, Integer> pos;
	public Entidad entidadPadre;
	public Celula(Tuple2D<Integer, Integer> posicion, Entidad entidadPadre) {
		pos = posicion;
		this.entidadPadre = entidadPadre;
	}

}
